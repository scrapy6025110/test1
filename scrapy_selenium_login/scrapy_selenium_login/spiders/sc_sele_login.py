import scrapy,time
# scrapy-seleniumではリクエストをSeleniumRequestで取得する
# 単体のrequestsライブラリーみたいな物
from scrapy_selenium import SeleniumRequest

# 画像などの取得の為の時間制御
# webDriverWait(driverに待ち時間を設定)
from selenium.webdriver.support.ui import WebDriverWait
# webDriverWaitにどこまで待つかを指定する際に使用
from selenium.webdriver.support import expected_conditions as EC

# WebDriverのインスタンスに直接find_element_by_idなどのメソッドは使用出来ない
# なので、find_elementで指定していくのに必要なByをインポート
from selenium.webdriver.common.by import By  # 必要なインポート文

# キーの特殊操作をするためにインポート
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.common.action_chains import ActionChains

# .envファイルを読み取るためのライブラリー
import os
from dotenv import load_dotenv

class ScSeleLoginSpider(scrapy.Spider):
    name = 'sc_sele_login'
    # allowed_domains = ['quotes.toscrape.com']
    # start_urls = ['http://quotes.toscrape.com/']
    
    '''scrapyのリクエストをオーバーライドする'''
    def start_requests(self):
         yield SeleniumRequest(
             # start_urlsプロパティの代わり。sを付ける
             # genspiderでスタートurlを間違っているがここで修正
             url = 'https://quotes.toscrape.com//login',
             # スクリーンショットを取得するか否か
             screenshot = False,
             # 処理関数の指定
             callback = self.parse,
         )

    '''webDriverWaitとexpected_conditionsを使って待ち時間を設定する関数'''
    # 引数はself,driverの指定,時間,By.＊＊＊の指定,XPathかCSSセレクターの指定
    def waittime_config(self,driver,time,by_method_type,xpath_or_css):
        # WebDriverWaitを使用して、ページの読み込みが完了するまで最大10秒待つ
        wait = WebDriverWait(driver, time)
        
        # bodyタグの要素を取得するまで待つ(結構使い勝手が良い)
        wait.until(EC.presence_of_element_located((by_method_type, xpath_or_css)))

    '''メインの処理関数'''
    def parse(self, response):
        # responseのmeta情報(辞書型データ)にはdriverが含まれているか否かで判定
        if 'driver' not in response.meta:
            self.logger.error("Driver not found in response meta. SeleniumRequest may not be working correctly.")
            print('driverは見つかりません!!')
            return

        # driverの設定(単独のseleniumより簡単)
        driver = response.meta['driver']
        
        # Webページのトップ画面がロードされるまで待機
        self.waittime_config(driver,3,By.CSS_SELECTOR,'body')
        
        # UsernameフォームのXPath  //input[@name="username"]
        # PasswordフォームのXPath  //input[@name="password"]
        
        # 入力フォームを定義
        user_form = driver.find_element(By.XPATH,'//input[@name="username"]')
        passwd_form = driver.find_element(By.XPATH,'//input[@name="password"]')
        
        # .envファイルからのユーザー情報の取得の為load_dotenvメソッドを呼び出す
        load_dotenv()
        
        # 各入力フォームに入力
        user_form.send_keys(os.getenv('USERNAME'))
        passwd_form.send_keys(os.getenv('USERNAME'))
        
        # 3秒待つ
        time.sleep(3)
        
        # スクショを取得
        driver.save_screenshot('fill_in_form.png')
        
        # 面倒臭いのでENTERを推して画面遷移する
        passwd_form.send_keys(Keys.ENTER)
        
        # 画面の遷移を3秒待ってスクショを撮ってみる
        
        self.waittime_config(driver,3,By.CSS_SELECTOR,'body')
        
        driver.save_screenshot('comform_changing_page.png')
        
        # ターミナルでも画面遷移しているか確かめる
        
        # logoutのXPath  //a[@href="/logout"]/text()
        
        if driver.find_element(By.XPATH,'//a[@href="/logout"]'):
            print('--------------------------------')
            print('ログインに成功しました')
            print('--------------------------------')
        else:
            print('--------------------------------')
            print('ログインに失敗しました')
            print('--------------------------------')

