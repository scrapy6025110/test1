# Scrapy settings for scrapy_selenium_login project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

# botの名前
BOT_NAME = 'scrapy_selenium_login'

SPIDER_MODULES = ['scrapy_selenium_login.spiders']
NEWSPIDER_MODULE = 'scrapy_selenium_login.spiders'

# このスパイダーからの出力はUTF-8にします
FEED_EXPORT_ENCODING = 'utf-8'

#User-Agentの中身(表示)を変える
#USER_AGENT = 'yodobashi (+http://www.yourdomain.com)'
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'scrapy_selenium_login (+http://www.yourdomain.com)'

# Obey robots.txt rules
ROBOTSTXT_OBEY = True

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#言語を日本語化
DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
  'Accept-Language': 'jp',
}

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'scrapy_selenium_login.middlewares.ScrapySeleniumLoginSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
# GitHubのscrapy-seleniumの設定を参照
DOWNLOADER_MIDDLEWARES = {
    'scrapy_selenium.SeleniumMiddleware': 800
}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
#ITEM_PIPELINES = {
#    'scrapy_selenium_login.pipelines.ScrapySeleniumLoginPipeline': 300,
#}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
# キャッシュを使います
HTTPCACHE_ENABLED = True
# HTTPキャッシュの保存期間(今回は1日で設定(秒))
# HTTPCACHE_EXPIRATION_SECS = 86400
# 不便なのでHTTPキャッシュを1週間にする
HTTPCACHE_EXPIRATION_SECS = 604800
# HTTPキャッシュの保存先
HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

# ---GitHubからscrapy-seleniumの設定を参照---

# from shutil import which

# SELENIUM_DRIVER_NAME = 'firefox'
SELENIUM_DRIVER_NAME = 'chrome'
# SELENIUM_DRIVER_EXECUTABLE_PATH = which('geckodriver')
# SELENIUM_DRIVER_EXECUTABLE_PATH = which('chromedriver')
# SELENIUM_DRIVER_EXECUTABLE_PATH = '/var/src1/bing/chromedriver' 
SELENIUM_DRIVER_EXECUTABLE_PATH = '/usr/local/bin/chromedriver'
# import os

# SELENIUM_DRIVER_EXECUTABLE_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'chromedriver')
# ヘッドレスモードの有無
# SELENIUM_DRIVER_ARGUMENTS=['--headless']  # '--headless' if using chrome instead of firefox
SELENIUM_DRIVER_ARGUMENTS=['--headless', '--no-sandbox', '--disable-dev-shm-usage'] 

# ---GitHubからscrapy-seleniumの設定を参照---

# ログレベルをDEBUGにする
LOG_LEVEL = 'DEBUG'