#jsonファイルからのデータを読み取るファイル

import json

with open('./data.json',mode='r',newline='',encoding='utf-8') as f:
    qiita_data = json.load(f)
    # print(qiita_data,type(qiita_data)) リスト-辞書型
    # print(qiita_data[0]['category']) #str
    # print(qiita_data[0]['titles']) #list
    # print(qiita_data[0]['urls']) #list
    
    #正直categoryはいらないのでtitles,urlsを変数とする
    titles = qiita_data[0]['titles']
    urls = qiita_data[0]['urls']
    
    for i,j in zip(titles,urls):
        print(i,'::',j)
    
    #教材はここで終わりますがここから新たなjsonやcsvを作成
    #してdbに格納することも可能

# import csv,sqlite3,time

# lists_dicts = [] 
# #後にdbに保存する為にcountを設定してidに渡す 
# count = 0 
# for i,j in zip(titles,urls): 
#     count += 1 
#     print(count,'::',i,'::',j) 
#     lists_dicts.append({'id':count,'title':i,'url':j}) 
#     time.sleep(0.3) 
#     if count == 30: 
#         break 

# file_path = 'test1.csv' 
# open_csv = open(file_path,mode='w',newline='',encoding='utf-8') 
# # writer = csv.DictWriter(open_csv) 
# fieldnames = ['id','title', 'url'] 
# writer = csv.DictWriter(open_csv, fieldnames=fieldnames) 
# writer.writeheader() 

# for i in lists_dicts: 
#     writer.writerow(i) 
#     print(i) 
#     time.sleep(0.3) 

 

# #SQLite3に格納する(テーブルはあるものとする) 
# db_path = 'open.db' 
# db = sqlite3.connect(db_path) 
# c = db.cursor() 

# sql = 'insert into test1scrapy values(?,?,?);' 

# c.executemany(sql,lists_dicts) 

# db.commit() 
# db.close 