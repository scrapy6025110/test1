import scrapy


class QiitaTrend1dSpider(scrapy.Spider):
    name = 'qiita_trend_1d' #スパイダーの名前(genspiderで定義したもの)
    allowed_domains = ['qiita.com'] #許可するドメイン
    # allowed_domains = ['quiita.com'] #許可するドメイン genspiderから作成したら間違っていた
    start_urls = ['https://qiita.com/'] #URLの指定 https通信ならsを追加
    # start_urls = ['https://quiita.com/'] #URLの指定 https通信ならsを追加 genspiderから作成したら間違っていた

    def parse(self, response): #ここのresponseでWebページの情報が格納されている
        #xpathでの取得
        # category = response.xpath("//div [@class='style-ka8n9l'] /a /text()").get()
        # titles = response.xpath('//h2[@class="style-skov52"] /a/text()').getall()
        # urls = response.xpath('//h2[@class="style-skov52"] /a/@href').getall()
        
        '''2024/08/03(土曜日)コード改修'''
        # ドキュメントページのトレンドの文字列を取得
        category = response.xpath('//a[@class="style-1f7ewg"]/text()').get()
        titles = response.xpath('//a[@class="style-2vm86z"]/text()').getall()
        urls = response.xpath('//a[@class="style-2vm86z"]/@href').getall()
        
        #cssセレクタでの取得
        # category = response.css('div[class="style-ka8n9l"] > a::text').get()
        # titles = response.css('h2[class="style-skov52"]  > a::text').getall()
        # urls = response.css('h2[class="style-skov52"] > a::attr(href)').getall()
        
        yield {
            'category':category,
            'titles':titles,
            'urls':urls,
        }
        
