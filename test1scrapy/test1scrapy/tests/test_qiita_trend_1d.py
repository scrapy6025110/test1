#pytestのテストコードを書いていきます



import pytest
import scrapy
from scrapy.http import TextResponse
from spiders.qiita_trend_1d import QiitaTrend1dSpider

#モジュールエラーが出たのでパスを調べた際の痕跡
#/var/src1/test1scrapy/test1scrapy

# import subprocess

# res = subprocess.run(['pwd'])
# print(res)

#解決はPYTHONPATH=/var/src1/test1scrapy/test1scrapy pytest
#とパスを明示することで解消(Dockerの場合(絶対パスで指定している))


#ローカルPCの場合はtest1/test1scrapy/test1scrapyディレクトリで
#PYTHONPATH=./ pytest tests/test_qiita_trend_1d.py --cov=spiders/ --cov-report term-missing -v -sとする
#相対パス


class TestQiitaTrend1dSpider(object):
    
    @classmethod
    def setup_class(cls):
        cls.qiita = QiitaTrend1dSpider()
        cls.name = 'qiita_trend_1d'
        cls.allowed_domains = ['qiita.com']
        cls.start_urls = ['https://qiita.com/']
        
    @classmethod
    def teardown_class(cls):
        del cls.qiita
        del cls.name
        del cls.allowed_domains
        del cls.start_urls
        
    def test_name(self):
        result = self.qiita.name
        print('------------------------')
        print('スパイダーの名前は{}'.format(result))
        print('------------------------')
        assert result == 'qiita_trend_1d'
        
    def test_qllowed_domains(self):
        result = self.qiita.allowed_domains
        print('------------------------')
        print('許可するドメインは{}'.format(result))
        print('------------------------')
        assert result == self.allowed_domains
        
    def test_start_urls(self):
        result = self.qiita.start_urls
        print('------------------------')
        print('クロールを開始するURLは{}'.format(result))
        print('------------------------')
        assert result == self.start_urls
    
    #----手動のモックを作成してのテスト-----------
    
    # def test_parse(self):
    #     # テスト用のHTMLレスポンスを作成
    #     html_content = """
    #     <html>
    #         <body>
    #             <div class="style-ka8n9l">
    #                 <a>Test Category</a>
    #             </div>
    #             <h2 class="style-skov52"><a href="https://www.org.Test_Url">Test Title</a></h2>
    #         </body>
    #     </html>
    #     """
    #     # モックのレスポンスオブジェクトを作成
    #     response = TextResponse('https://example.com', body=html_content, encoding='utf-8')
        
    #     # Spiderのparseメソッドを呼び出してテスト
    #     parsed_data = list(self.qiita.parse(response))
        
    #     print('parsed_dataは以下の通りです{}'.format(parsed_data))
        
    #     assert parsed_data[0]['category'] == 'Test Category'
    #     assert parsed_data[0]['titles'] == ['Test Title']
    #     assert parsed_data[0]['urls'] == ['https://www.org.Test_Url']
    
    # #最初に自作で作ったコード(モックがないのでエラー)
    #     #spiders/qiita_trend_1d.pyのparse関数を呼び出す
    #     def test_parse(self,response):
    #         self.qiita.parse(self,response)
    #         result_category = self.category = response.xpath("//div [@class='style-ka8n9l'] /a /text()").get()
    #         result_title = self.titles = response.xpath('//h2[@class="style-skov52"] /a/text()').getall()
    #         result_urls = self.urls = response.xpath('//h2[@class="style-skov52"] /a/@href').getall()
            
    #         assert result_category
    #         assert result_title
    #         assert result_urls    
            
    #----手動のモックを作成してのテスト-----------
    
    #----monkeypatchを使ったコード-------------


    # モックの関数の定義
    def get_response_mock(response):
        # テスト用のHTMLレスポンスを作成
        # html_content = """
        # <html>
        #     <body>
        #         <div class="style-ka8n9l">
        #             <a>Test Category</a>
        #         </div>
        #         <h2 class="style-skov52"><a href="https://www.org.Test_Url">Test Title</a></h2>
        #     </body>
        # </html>
        # """
        
        # 2024/08/03(土)QiitaのサイトのXPathが変更になったのでhtml_contentを変更
        html_content = """
        <html>
            <body>
                <a class="style-1f7ewg">Test Category</a>
                <a class="style-2vm86z" href="https://www.org.Test_Url">Test Title</a>
            </body>
        </html>
        """
        
        # モックのレスポンスオブジェクトを作成
        response = TextResponse('https://example.com', body=html_content, encoding='utf-8')
        return response

    # モンキーパッチを使用してparseメソッドを置き換える
    @pytest.fixture
    def parse_monkeypatch(monkeypatch):
        # モックの定義をモンキーパッチに適用
        monkeypatch.setattr(
            QiitaTrend1dSpider,
            'parse',
            lambda self, response: get_response_mock(response)
        )

    # テストメソッド
    def test_parse(parse_monkeypatch):
        # テスト用のHTMLレスポンスを作成
        # html_content = """
        # <html>
        #     <body>
        #         <div class="style-ka8n9l">
        #             <a>Test Category</a>
        #         </div>
        #         <h2 class="style-skov52"><a href="https://www.org.Test_Url">Test Title</a></h2>
        #     </body>
        # </html>
        # """
        
        # 2024/08/03(土)QiitaのサイトのXPathが変更になったのでhtml_contentを変更
        html_content = """
        <html>
            <body>
                <a class="style-1f7ewg">Test Category</a>
                <a class="style-2vm86z" href="https://www.org.Test_Url">Test Title</a>
            </body>
        </html>
        """
        
        # インスタンス化
        spider = QiitaTrend1dSpider()

        # テスト用のダミーレスポンスを作成してparseメソッドに渡す
        response = TextResponse(url='http://example.com', body=html_content, encoding='utf-8')
        parsed_data = list(spider.parse(response))

        #検証用
        print('------------------------')
        print('parsed_dataは{}です'.format(parsed_data))
        print('------------------------')

        # テスト
        assert parsed_data[0]['category'] == 'Test Category'
        assert parsed_data[0]['titles'] == ['Test Title']
        assert parsed_data[0]['urls'] == ['https://www.org.Test_Url']




    

    # モックの関数の定義
    # def get_response_mock(response):
    #     # テスト用のHTMLレスポンスを作成
    #     html_content = """
    #     <html>
    #         <body>
    #             <div class="style-ka8n9l">
    #                 <a>Test Category</a>
    #             </div>
    #             <h2 class="style-skov52"><a href="https://www.org.Test_Url">Test Title</a></h2>
    #         </body>
    #     </html>
    #     """
    #     # モックのレスポンスオブジェクトを作成
    #     # response._set_body(html_content.encode('utf-8'))
    #     response = TextResponse('https://example.com', body=html_content, encoding='utf-8')
    #     return response

    # # モンキーパッチを使用してparseメソッドを置き換える
    # def test_parse(monkeypatch):
    #     # モックの定義
    #     monkeypatch.setattr(
    #         QiitaTrend1dSpider,
    #         'parse',
    #         lambda self, response: get_response_mock(response)
    #     )

    #     # インスタンス化
    #     spider = QiitaTrend1dSpider()

    #     # parseメソッドの呼び出し
    #     response = None  # ここに本来のレスポンスオブジェクトを渡す必要があります
    #     parsed_data = list(spider.parse(response))

    #     # テスト
    #     assert parsed_data[0]['category'] == 'Test Category'
    #     assert parsed_data[0]['titles'] == ['Test Title']
    #     assert parsed_data[0]['urls'] == ['https://www.org.Test_Url']



    
    #----monkeypatchを使ったコード-------------
