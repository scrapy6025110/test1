# Dockerコンテナ内でseleniumでの無限スクロールサイトのクローラーです

import time,datetime,csv,requests
# from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.service import Service

#WebDriverのインスタンスに直接find_element_by_idなどのメソッドは使用出来ない
#なので、find_elementで指定していくのに必要なByをインポート
from selenium.webdriver.common.by import By  # 必要なインポート文

# キーの特殊操作をするためにインポート
from selenium.webdriver.common.keys import Keys

# -----ヘッドレスモードの利用------------------

# selenium.webdriver.chrome.optionsからクラスOptionsを呼び出す
from selenium.webdriver.chrome.options import Options
# クラスOptions()をインスタンス化
options = Options()
# add_argumentメソッドの引数に--headlessを追加してヘッドレスモードを定義
# ここをコメントアウトするとヘッドレスモードから通常モードへ
options.add_argument('--headless')
# 追記
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

# -----ヘッドレスモードの利用------------------

'''selenium4系の書き方'''
# #chromedriverの指定とdriverの作成
# service = Service('/usr/local/bin/chromedriver')
# # serviceでWebドライバーの指定、optionsでオプション(今回はヘッドレスモードの有無)を指定
# driver = webdriver.Chrome(service=service,options=options)
'''selenium4系の書き方'''

'''selenium3系の書き方'''
# ChromeDriverのパスを指定
chromedriver_path = '/usr/local/bin/chromedriver'

# WebDriverの初期化
driver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=options)
'''selenium3系の書き方'''

# 今回クロールする無限スクロールサイト
# antennaの徳島の検索結果の無限スクロール(キーワードが特殊文字(徳島)になっているが気にしない)
url = 'https://antenna.jp/articles?keyword=%E5%BE%B3%E5%B3%B6&commit='

driver.get(url)

# スクショを撮ってみる
driver.save_screenshot('first_screenshot.png')

#ブラウザのウインドウ高を取得する
win_height = driver.execute_script("return window.innerHeight")
print('-------------------------------')
print(f'ブラウザのウインドウ高は {win_height} です')
print('-------------------------------')

#スクロール開始位置の初期値（ページの先頭からスクロールを開始する）
last_top = 1

#ページの最下部までスクロールする無限ループ
while True:
    

    #スクロール前のページの高さを取得
    last_height = driver.execute_script("return document.body.scrollHeight")
    print('-------------------------------')
    print(f'スクロール前の高さは {last_height} です')
    print('-------------------------------')
    
    #スクロール開始位置を設定
    top = last_top

    #ページ最下部まで、徐々にスクロールしていく
    while top < last_height:
        top += int(win_height * 0.8)
        # driver.execute_script("window.scrollTo(0, %d)" % top)
        driver.find_element(By.TAG_NAME,'body').send_keys(Keys.END)
        time.sleep(0.5)

    #１秒待って、スクロール後のページの高さを取得する
    time.sleep(1)
    new_last_height = driver.execute_script("return document.body.scrollHeight")
    print('-------------------------------')
    print(f'スクロール後のページの高さは {new_last_height} です')
    print('-------------------------------')

    #スクロール前後でページの高さに変化がなくなったら無限スクロール終了とみなしてループを抜ける
    if last_height == new_last_height:
        break

    #次のループのスクロール開始位置を設定
    last_top = last_height

# 再度スクショを取得
# 幅と高さを指定していないので一番下のスクショを取得
driver.save_screenshot('after_screenshot.png')
