# Scrapy-Seleniumでの画面スクロールが上手くいかなかったのでseleniumでのコーディング

import time,datetime,csv,requests,json
# from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.service import Service

#WebDriverのインスタンスに直接find_element_by_idなどのメソッドは使用出来ない
#なので、find_elementで指定していくのに必要なByをインポート
from selenium.webdriver.common.by import By  # 必要なインポート文

# キーの特殊操作をするためにインポート
from selenium.webdriver.common.keys import Keys

# 画像などの取得の為の時間制御
# webDriverWait(driverに待ち時間を設定)
from selenium.webdriver.support.ui import WebDriverWait
# webDriverWaitにどこまで待つかを指定する際に使用
from selenium.webdriver.support import expected_conditions as EC

# -----ヘッドレスモードの利用------------------

# selenium.webdriver.chrome.optionsからクラスOptionsを呼び出す
from selenium.webdriver.chrome.options import Options
# クラスOptions()をインスタンス化
options = Options()
# add_argumentメソッドの引数に--headlessを追加してヘッドレスモードを定義
# ここをコメントアウトするとヘッドレスモードから通常モードへ
options.add_argument('--headless')
# 追記
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

'''***ヘッドレスモードを解除して表示するにはx等が必要***'''

# -----ヘッドレスモードの利用------------------

'''selenium4系の書き方'''
# #chromedriverの指定とdriverの作成
# service = Service('/usr/local/bin/chromedriver')
# # serviceでWebドライバーの指定、optionsでオプション(今回はヘッドレスモードの有無)を指定
# driver = webdriver.Chrome(service=service,options=options)
'''selenium4系の書き方'''

'''selenium3系の書き方'''
# ChromeDriverのパスを指定
chromedriver_path = '/usr/local/bin/chromedriver'

# WebDriverの初期化
driver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=options)
'''selenium3系の書き方'''

# 今回クロールする最初のサイトのURL(Antennaのトップページ)
url = 'https://antenna.jp/'

# driverでトップページのレスポンスを取得
driver.get(url)

'''webDriverWaitとexpected_conditionsを使って待ち時間を設定する関数'''

# 引数はself,driverの指定,時間,By.＊＊＊の指定,XPathかCSSセレクターの指定
def waittime_config(driver,time,by_method_type,xpath_or_css):
    # WebDriverWaitを使用して、ページの読み込みが完了するまで最大10秒待つ
    wait = WebDriverWait(driver, time)
        
    # bodyタグの要素を取得するまで待つ(結構使い勝手が良い)
    wait.until(EC.presence_of_element_located((by_method_type, xpath_or_css)))

'''webDriverWaitとexpected_conditionsを使って待ち時間を設定する関数'''
    
'''トップページのスクショ~検索窓に'徳島'と入力->スクショ->画面遷移->スクショまで'''

# トップページのスクショを取得
driver.save_screenshot('antena_top_page.png')

# 検索窓のXPath  //input[@id="search-input"]

# 検索窓を指定
search_box = driver.find_element(By.XPATH,'//input[@id="search-input"]')

# 検索窓に徳島と入力
search_box.send_keys('徳島')

# 2秒待つ
waittime_config(driver,2,By.CSS_SELECTOR,'body')

# 検索窓に徳島と入力されているかスクショを撮って確認
driver.save_screenshot('what_do_fill_in_search_window.png')

# 画面遷移する
# 検索決定ボタンをXPathで変数に格納 //input[@type="submit"]
# submitとする
search_box.submit() # OK
# メガネマークをクリック
# megane_mark = driver.find_element(By.XPATH,'//input[@type="submit"]')
# megane_mark.click()
# エンターキーを押す
# search_box.send_keys(Keys.ENTER) # OK

# 5秒待つ
waittime_config(driver,5,By.CSS_SELECTOR,'body')

# スクショの取得
driver.save_screenshot('tokushima_first_page.png')

'''トップページのスクショ~検索窓に'徳島'と入力->スクショ->画面遷移->スクショまで'''

'''画面のスクロール->画面の幅と高さを定義して画面全体のスクショ->'''

#ブラウザのウインドウ高を取得する
win_height = driver.execute_script("return window.innerHeight")
print('-------------------------------')
print(f'ブラウザのウインドウ高は {win_height} です')
print('-------------------------------')

#スクロール開始位置の初期値（ページの先頭からスクロールを開始する）
last_top = 1

#ページの最下部までスクロールする無限ループ
while True:
    

    #スクロール前のページの高さを取得
    last_height = driver.execute_script("return document.body.scrollHeight")
    print('-------------------------------')
    print(f'スクロール前の高さは {last_height} です')
    print('-------------------------------')
    
    #スクロール開始位置を設定
    top = last_top

    #ページ最下部まで、徐々にスクロールしていく
    while top < last_height:
        # win_heightの80%をtopに足す
        top += int(win_height * 0.8)
        # topの分だけ画面スクロールする
        driver.execute_script("window.scrollTo(0, %d)" % top)
        # 一気にスクロールさせるならこちらでも良い
        # driver.find_element(By.TAG_NAME,'body').send_keys(Keys.END)
        # waittime_config(driver,0.5,By.CSS_SELECTOR,'body')
        time.sleep(0.5)

    #１秒待って、スクロール後のページの高さを取得する
    # waittime_config(driver,1,By.CSS_SELECTOR,'body')
    time.sleep(1)
    new_last_height = driver.execute_script("return document.body.scrollHeight")
    print('-------------------------------')
    print(f'スクロール後のページの高さは {new_last_height} です')
    print('-------------------------------')

    #スクロール前後でページの高さに変化がなくなったら無限スクロール終了とみなしてループを抜ける
    if last_height == new_last_height:
        break

    #次のループのスクロール開始位置を設定
    last_top = last_height

# 再度スクショを取得
w = driver.execute_script('return document.body.scrollWidth')
h = driver.execute_script('return document.body.scrollHeight')
driver.set_window_size(w,h)

# 幅と高さを指定していないので一番下のスクショを取得
driver.save_screenshot('full_down_tokushima_first_page.png')

'''画面のスクロール->画面の幅と高さを定義して画面全体のスクショ->'''

'''要素(タイトルとURL)の取得'''
# タイトルのXPath   //div[@class="text"]//div[@class="title"]
# URLのXPath       //div[@class="articles"]//a[@class="thumbnail-content"]
        
# タイトルのオブジェクト
t_objects = driver.find_elements(By.XPATH,'//div[@class="text"]//div[@class="title"]')
        
#URLの取得
u_objects = driver.find_elements(By.XPATH,'//div[@class="articles"]//a[@class="thumbnail-content"]')
        
        
# 出力
# for i in t_objects:
#     print(i.text)
            
# for j in u_objects:
#     print('-----------URL---------------')
#     print(j.get_attribute('href'))
#     # url = j.get_attribute('href')
#     # print(url)
#     print('-----------------------------')
        
for i,j in zip(t_objects,u_objects):
    title = i.text
    url = j.get_attribute('href')
    print('-----------タイトルとURL---------------')
    print(f'タイトルは :: {title} // URLは :: {url} です')
    print('-------------------------------------')


'''要素(タイトルとURL)の取得'''

'''JSON/CSVファイルを作成するコード'''

# ファイルネームを指定(JSON)
json_file_name = 'result.json'

# ファイルを作成(CSV)
csv_file_name = 'result.csv'

# # ファイルを設定を記述(自作)
# with open (file=csv_file_name,mode='w',newline='',encoding='utf-8') as f:
#     writer = csv.writer(f)
#     for i,j in zip(t_objects,u_objects):
#         title = i.text
#         url = j.get_attribute('href')
#         # リスト型にして渡すwriterowは1つの引数しか取れない
#         writer.writerow([title,url])

# データを格納するリストを作成
lists = []

# データを収集
for i, j in zip(t_objects, u_objects):
    title = i.text
    url = j.get_attribute('href')
    lists.append({"title": title, "url": url})

# 検証用
# for i in lists:
#     print(i.values())
#     print('--------------------------------------------------------')
#     print(f'タイトルは {i["title"]} // URLは{i["url"]} です')

# JSONファイルに書き込み
with open(file=json_file_name, mode='w',newline='' ,encoding='utf-8') as f:
    json.dump(lists, f, ensure_ascii=False, indent=4)

print(f"データを {json_file_name} に保存しました。")

# CSVファイルに書き込み
with open(file=csv_file_name,mode='w',newline='',encoding='utf-8') as f:
    writer = csv.writer(f)
    
    # ヘッダーを書き込む
    # リスト型にして渡すwriterowは1つの引数しか取れない
    writer.writerow(['Title','URL'])

    # titleとurlをCSVファイルに書き込んでいく
    for i, j in zip(t_objects, u_objects):
        title = i.text
        url = j.get_attribute('href')
        # リスト型にして渡すwriterowは1つの引数しか取れない
        writer.writerow([title,url])

# # CSVファイルの書き込み(DictWriterを使ったバージョン)
# with open(file=csv_file_name,mode='w',newline='',encoding='utf-8') as f:
#     writer = csv.DictWriter(f,fieldnames=['title','url'])
    
#     # ヘッダーを書き込む(DictWriterで定義しているので引数を取らない)
#     writer.writeheader()
    
#     # listsをCSVファイルに書き込んでいく
#     writer.writerow(lists)

print(f'CSVファイル {csv_file_name} を作成しました')
        


'''JSON/CSVファイルを作成するコード'''
