import scrapy,time
# scrapy-seleniumではリクエストをSeleniumRequestで取得する
# 単体のrequestsライブラリーみたいな物
from scrapy_selenium import SeleniumRequest

# 画像などの取得の為の時間制御
# webDriverWait(driverに待ち時間を設定)
from selenium.webdriver.support.ui import WebDriverWait
# webDriverWaitにどこまで待つかを指定する際に使用
from selenium.webdriver.support import expected_conditions as EC

# WebDriverのインスタンスに直接find_element_by_idなどのメソッドは使用出来ない
# なので、find_elementで指定していくのに必要なByをインポート
from selenium.webdriver.common.by import By  # 必要なインポート文

# キーの特殊操作をするためにインポート
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.common.action_chains import ActionChains


class AntennaTokushimaSpider(scrapy.Spider):
    name = 'antenna_tokushima'
    # allowed_domains = ['antenna.jp']
    # start_urls = ['http://antenna.jp/']
    
    '''scrapyのリクエストをオーバーライドする'''
    def start_requests(self):
         yield SeleniumRequest(
             # start_urlsプロパティの代わり。sを付ける
             url = 'https://antenna.jp/',
             # スクリーンショットを取得するか否か
             screenshot = False,
             # 処理関数の指定
             callback = self.parse,
         )

    # def scroll_down_action(self,scroll,driver):
    #     actions = ActionChains(driver)
    #     for _ in range(scroll):  # 引数にスクロール回数を指定
    #         actions.send_keys(Keys.PAGE_DOWN).perform()
    #         time.sleep(10)  # スクロール後に少し待機
    
    def scroll_down_action(self, driver):
        # ページ全体の高さを取得
        page_height = driver.execute_script('return document.body.scrollHeight')

        # ページ全体の高さ分だけスクロール
        for _ in range(page_height // 100 + 1):
            # driver.find_element(By.TAG_NAME, 'body').send_keys(Keys.PAGE_DOWN)
            driver.find_element(By.XPATH, '//body//div/script').send_keys(Keys.PAGE_DOWN)
            time.sleep(5)
    
    # def scroll_down_action(self, driver):
    #     before_h = driver.execute_script('return document.body.scrollHeight')
    #     print(f'現在の高さは {before_h} です')
    #     time.sleep(10)
    #     # JavaScriptを使用して、ページ最下部までスクロール
    #     driver.execute_script('window.scrollTo(0, document.body.scrollHeight)')
    #     time.sleep(10)
    #     after_h = driver.execute_script('return document.body.scrollHeight')
    #     print(f'PAGE_DOWN後は {after_h} です')
    #     # スクショの保存
    #     driver.save_screenshot('お願い.png')
    
    '''webDriverWaitとexpected_conditionsを使って待ち時間を設定する関数'''
    # 引数はself,driverの指定,時間,By.＊＊＊の指定,XPathかCSSセレクターの指定
    def waittime_config(self,driver,time,by_method_type,xpath_or_css):
        # WebDriverWaitを使用して、ページの読み込みが完了するまで最大10秒待つ
        wait = WebDriverWait(driver, time)
        
        # bodyタグの要素を取得するまで待つ(結構使い勝手が良い)
        wait.until(EC.presence_of_element_located((by_method_type, xpath_or_css)))

    def parse(self, response):
        # responseのmeta情報(辞書型データ)にはdriverが含まれているか否かで判定
        if 'driver' not in response.meta:
            self.logger.error("Driver not found in response meta. SeleniumRequest may not be working correctly.")
            print('driverは見つかりません!!')
            return

        # driverの設定(単独のseleniumより簡単)
        driver = response.meta['driver']
        
        # スクショの画面の幅と高さを取得するためにJSを実行して幅と高さを取得してdriverに定義する
        w = driver.execute_script('return document.body.scrollWidth')
        h = driver.execute_script('return document.body.scrollHeight')
        driver.set_window_size(w,h)
        
        # 関数waittime_configを使って待ち時間を指定
        # 今回はdriverを使って10待ちcssセレクターでbody要素が読み込まれるまで待ちます
        # self.waittime_config(driver,10,By.CSS_SELECTOR,'body')
        time.sleep(10)
        
        # antennaのトップページのスクショを取得
        driver.save_screenshot('first_top_page.png')
        
        '''---検索窓に徳島と入力してスクショを取得して確定する---'''
        
        # 検索窓のXPath  //input[@id="search-input"]
        # 検索窓を指定
        search_box = driver.find_element(By.XPATH,'//input[@id="search-input"]')
        # 検索窓に徳島と入力
        search_box.send_keys('徳島')
        # 2秒待つ
        # self.waittime_config(driver,2,By.CSS_SELECTOR,'body')
        time.sleep(5)
        # 徳島と検索窓に入力されているかスクショを取得
        driver.save_screenshot('fillout_tokushima.png')
        
        '''---検索窓に徳島と入力してスクショを取得して確定する---'''
        
        '''===========検索を決定して検索画面を表示============'''
        
        # 徳島と入力後決定をする
        # 検索決定ボタンをXPathで変数に格納 //input[@type="submit"]
        # submitとする
        search_box.submit() # OK
        # メガネマークをクリック
        # megane_mark = driver.find_element(By.XPATH,'//input[@type="submit"]')
        # megane_mark.click()
        # エンターキーを押す
        # search_box.send_keys(Keys.ENTER) # OK
        
        
        
        # 待機時間10秒
        # self.waittime_config(driver,10,By.CSS_SELECTOR,'body')
        time.sleep(10)
        
        # 検索結果のスクショ
        driver.save_screenshot('search_result_tokushima.png')
        
        '''===========検索を決定して検索画面を表示============'''
        
        '''----------画面スクロールしてスクショをとる----------'''
        
        # ****************** 注釈 ***************************
        # * こちらの環境では画面スクロールしないので、親ディレクトリ *
        # * 配下にunit_crollerディレクトリを作成してseleniumのク *
        # * ローラーでの処理で進めていきますが、環境が合えばこちらの *
        # * Scrapy-Seleniumで画面スクロールすると思われます。    *
        # ****************** 注釈 ***************************
        
        
        # ブラウザのウインドウ高を取得する
        win_height = driver.execute_script("return window.innerHeight")
        print('-------------------------------')
        print(f'ブラウザのウインドウ高は {win_height} です')
        print('-------------------------------')

        # スクロール開始位置の初期値（ページの先頭からスクロールを開始する）
        last_top = 1

        # ページの最下部までスクロールする無限ループ
        while True:
            

            # スクロール前のページの高さを取得
            last_height = driver.execute_script("return document.body.scrollHeight")
            print('-------------------------------')
            print(f'スクロール前の高さは {last_height} です')
            print('-------------------------------')
            
            # スクロール開始位置を設定
            top = last_top

            # ページ最下部まで、徐々にスクロールしていく
            while top < last_height:
                top += int(win_height * 0.8)
                # top += int(win_height * 0.6)
                # top += int(win_height * 0.9)
                driver.execute_script("window.scrollTo(0, %d)" % top)
                time.sleep(5)
            
            # 新しいbody要素を取り込むまで10秒待つ
            self.waittime_config(driver,10,By.CSS_SELECTOR,'body footer')
            print('-------------------------------')
            print('新しいbody要素を取り込みました')
            print('-------------------------------')

            # １0秒待って、スクロール後のページの高さを取得する
            time.sleep(10)
            new_last_height = driver.execute_script("return document.body.scrollHeight")
            print('-------------------------------')
            print(f'スクロール後のページの高さは {new_last_height} です')
            print('-------------------------------')

            # スクロール前後でページの高さに変化がなくなったら無限スクロール終了とみなしてループを抜ける
            if last_height == new_last_height:
                break

            # 次のループのスクロール開始位置を設定
            last_top = last_height
        
        '''教材のノーマルのコード'''
        # for i in range(10):
        #     driver.find_element(By.TAG_NAME,'body').send_keys(Keys.END)
        #     time.sleep(5)
        '''教材のノーマルのコード'''
        
        '''その他自身、AIで試したこと'''
        # 一度クリックする
        # driver.find_element(By.CSS_SELECTOR,'body').click()
        
        # # 
        # driver.find_element(By.CSS_SELECTOR,'body').send_keys(Keys.END)
        # driver.find_element(By.TAG_NAME,'body').send_keys(Keys.END)
        
        #
        # driver.find_element(By.TAG_NAME,'body').click()
        # driver.find_element(By.TAG_NAME,'body').submit() # ダメ
        # driver.find_element(By.XPATH,'//input[@type="submit"]').click()
        # driver.find_element(By.XPATH,'//input[@type="submit"]').send_keys(Keys.ENTER)
        # driver.find_element(By.XPATH,'//input[@id="search-input"]').click()
        # driver.find_element(By.XPATH,'//input[@id="search-input"]').submit()
        
        # time.sleep(5)
        # driver.find_element(By.TAG_NAME,'body').send_keys(Keys.PAGE_DOWN)
        # driver.find_element(By.TAG_NAME,'body').send_keys(Keys.END)
        
        # driver.find_element_by_tag_name('body').click()
        # driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN)
        
        # # スクロール前後のページ位置を確認
        # scroll_position_before = driver.execute_script("return window.pageYOffset;")
        # # driver.find_element(By.TAG_NAME,'body').click()
        # driver.find_element(By.TAG_NAME, 'body').send_keys(Keys.END)
        # # ページ最下部へスクロール
        # driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        # # または特定の距離だけスクロール
        # # driver.execute_script("window.scrollBy(0, 500);")
        # driver.set_window_size(788, 9999)  # 適切なサイズに調整
        # time.sleep(2)  # スクロールの完了を待つ
        # scroll_position_after = driver.execute_script("return window.pageYOffset;")
        # print(f"Scroll position: Before = {scroll_position_before}, After = {scroll_position_after}")     
        
        # # # 10秒待つ
        # # self.waittime_config(driver,10,By.CSS_SELECTOR,'body')
        # time.sleep(5)
        
        # # # 実行後search_result_tokushima.pngと比較するためにスクショを取得
        # driver.save_screenshot('compare_search_result_tokushima1.png')
        
        # # for文で回す
        # for i in range(1,3):
        #     # driver.find_element(By.CSS_SELECTOR,'body').click()
        #     # driver.find_element(By.CSS_SELECTOR,'body').send_keys(Keys.END)
        #     driver.find_element(By.TAG_NAME,'body').click()
        #     WebDriverWait(driver, 10).until(
        #         EC.presence_of_element_located((By.CSS_SELECTOR, '.expected-new-element'))
        #     )
        #     # time.sleep(5)
        #     driver.find_element(By.TAG_NAME,'body').send_keys(Keys.PAGE_DOWN)
        #     time.sleep(5)
        #     w = driver.execute_script('return document.body.scrollWidth')
        #     h = driver.execute_script('return document.body.scrollHeight')
        #     print(f'{i}回目です')
        #     print(f'幅は{w}、高さは{h}です')
            # time.sleep(10)
            # driver.save_screenshot(f'{i}回目実行のスクショ.png')
        
        # # for文で回す
        # for i in range(1,30):
        #     driver.find_element(By.CSS_SELECTOR,'body').send_keys(Keys.DOWN)
        #     time.sleep(10)
        #     # driver.save_screenshot(f'{i}回目実行のスクショ.png')
        
        # 実行後search_result_tokushima.pngと比較するためにスクショを取得
        driver.save_screenshot('compare_search_result_tokushima2.png')
        
        # last_height = driver.execute_script("return document.body.scrollHeight")
        # num = 1
        # while True:
        #     # スクロール前の位置をログに出力
        #     scroll_before = driver.execute_script("return window.scrollY;")
        #     print(f'Scroll position before scroll: {scroll_before}')

        #     # JavaScript でスクロールを試みる
        #     driver.execute_script("window.scrollBy(0, document.body.scrollHeight);")
        #     # time.sleep(5)  # スクロール後に少し長めに待機してみます
        #     self.waittime_config(driver,10,By.CSS_SELECTOR,'body')
            
        #     # スクロール後の位置をログに出力
        #     scroll_after = driver.execute_script("return window.scrollY;")
        #     print(f'Scroll position after scroll: {scroll_after}')

        #     new_height = driver.execute_script("return document.body.scrollHeight")
        #     print(f'new height: {new_height}, last height: {last_height}')

        #     if new_height == last_height:
        #         print(f'{num}回目でのwhileのブレイクです')
        #         break
        #     last_height = new_height
        #     driver.save_screenshot(f'scroll_{num}.png')
        #     num += 1

        # driver.save_screenshot('final_scroll.png')
        
        # last_height = driver.execute_script("return document.body.scrollHeight")
        # num = 1
        # while True:
        #     # スクロール前の位置をログに出力
        #     scroll_before = driver.execute_script("return window.scrollY;")
        #     print(f'Scroll position before scroll: {scroll_before}')

        #     # JavaScript でスクロールを試みる
        #     # 特定の要素にスクロールする方法に変更
        #     driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        #     time.sleep(5)  # スクロール後に少し長めに待機してみます

        #     # スクロール後の位置をログに出力
        #     scroll_after = driver.execute_script("return window.scrollY;")
        #     print(f'Scroll position after scroll: {scroll_after}')

        #     new_height = driver.execute_script("return document.body.scrollHeight")
        #     print(f'new height: {new_height}, last height: {last_height}')

        #     if new_height == last_height:
        #         print(f'{num}回目でのwhileのブレイクです')
        #         break
        #     last_height = new_height
        #     driver.save_screenshot(f'scroll_{num}.png')
        #     num += 1

        # driver.save_screenshot('final_scroll.png')
        
        # scroll_down_action関数の使用
        # self.scroll_down_action(10,driver)
        # self.scroll_down_action(driver)
        # driver.save_screenshot(f'アクションチェーンでスクロール後のスクショ.png')
        '''その他自身、AIで試したこと'''
        
        '''----------画面スクロールしてスクショをとる----------'''
        
        '''============記事のタイトルとURLを取得============='''
        
        # タイトルのXPath   //div[@class="text"]//div[@class="title"]
        # URLのXPath       //div[@class="articles"]//a[@class="thumbnail-content"]
        
        # タイトルのオブジェクト
        t_objects = driver.find_elements(By.XPATH,'//div[@class="text"]//div[@class="title"]')
        
        #URLの取得
        u_objects = driver.find_elements(By.XPATH,'//div[@class="articles"]//a[@class="thumbnail-content"]')
        
        
        # 出力
        for i in t_objects:
            print(i.text)
            
        for j in u_objects:
            print('-----------URL---------------')
            print(j.get_attribute('href'))
            # url = j.get_attribute('href')
            # print(url)
            print('-----------------------------')
        
        for i,j in zip(t_objects,u_objects):
            title = i.text
            url = j.get_attribute('href')
            yield{
                'title': title,
                'url': url,
            }
        
        '''============記事のタイトルとURLを取得============='''


    
