# from urllib import request
import scrapy
#ログを出力するライブラリー
import logging

class DesktopSpider(scrapy.Spider):
    #スパイダーの名前
    name = 'desktop'
    #ドメイン名なので/以下は削除
    allowed_domains = ['www.yodobashi.com']
    #httpsにする
    # start_urls = ['https://www.yodobashi.com/category/19531/11970/34646/']
    start_urls = ['https://www.yodobashi.com/category/19531/11970/34646']

    def parse(self, response):
        
        #UserAgent検証用のコード(検証終わったらコメントアウト)
        # logging.info(response.request.headers['User-Agent'])
        
        # まずは全体のデータを取得してその中の要素(メーカー名・商品名・価格)を抽出
        all_elements = response.xpath('//div[contains(@class,"productListTile")]' )
        #CSSセレクタならこうなります
        # all_elements = response.css('div.productListTile ')
        
        #responseの中からURLの情報をログとして取得
        logging.info(response.url)
        
        for i in all_elements:
            #all_elementsはセレクターオブジェクトなのでXPathで指定の場合は'.(ドット)'をつける
            #all_elementsからのパス(カレントディレクトリ)と解釈しても良いです。よって'.(ドット)'を付ける解釈でも良い
            #また最後に指定した要素を取得するので.get()メソッドを追加する
            maker = i.xpath('.//div[@class="pName fs14"]/p[1]/text()').get()
            #CSSセレクタならこのようになります
            #CSSセレクタの場合は'.(ドット)'を付ける必要はありません
            #また最後に指定した要素を取得するので.get()メソッドを追加する
            # maker = i.css('div.pName.fs14 > p::text').get()
            goods = i.xpath('.//div[@class="pName fs14"]/p[2]/text()').get()
            price = i.xpath('.//span[@class="productPrice"]/text()').get()
            
            #---検証用---
            # print('-----------')
            # print('メーカーは::',maker,'です')
            # print('商品とスペックは::',goods,'です')
            # print('価格は::',price,'です')
            # print('-----------')
            #---検証用---
            
            #yieldで戻り値を返す
            yield {
            'maker':maker,
            'goods':goods,
            'price':price,
            }
        
        #次へのページのhref属性を取得
        next_page = response.xpath('//a[@class="next"]/@href').get()
        #ページが遷移していくといつかnext_pageは無くなるのでそれを利用
        if next_page:
            #next_pageのURLに遷移してparse関数を次々実行
            #parse関数はコールバック関数として動作する
            yield response.follow(url=next_page,callback=self.parse)
            
            #response.followはa要素のセレクタを渡すと自動でURLを抽出する
            #その場合next_page = response.xpath('//a[@class="next"]')となる
            #また、.followの第一引数はリストの最初のインデックス番号0を渡す url=next_page[0]
            
        
