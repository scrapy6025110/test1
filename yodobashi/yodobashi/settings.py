# Scrapy settings for yodobashi project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

#ボットの名前(スパイダーの名前では無い)
BOT_NAME = 'yodobashi'

SPIDER_MODULES = ['yodobashi.spiders']
NEWSPIDER_MODULE = 'yodobashi.spiders'

#文字コードの追加
FEED_EXPORT_ENCODING = 'utf-8'


# Crawl responsibly by identifying yourself (and your website) on the user-agent

#User-Agentの中身(表示)を変える
# USER_AGENT = 'yodobashi (+http://www.yourdomain.com)'
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'
# USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'

# Obey robots.txt rules
#サーバーにrobots.txtがあればそれに従うか否か
ROBOTSTXT_OBEY = True
# ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
# 平行処理の数。デフォルト16。多くすると処理は早くなるが対象サーバーに負荷が増える。
# CONCURRENT_REQUESTS = 32
# CONCURRENT_REQUESTS = 16
# CONCURRENT_REQUESTS = 1

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#クローラーを当てる間隔を指定(秒)
DOWNLOAD_DELAY = 3
# DOWNLOAD_DELAY = 5
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# リトライ回数を増やす(デフォルト3)
# RETRY_TIMES = 5
# RETRY_TIMES = 2

# タイムアウト時間を設定 default 180秒
# DOWNLOAD_TIMEOUT = 60
# DOWNLOAD_TIMEOUT = 90
# DOWNLOAD_TIMEOUT = 20

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#言語設定を日本語にする
DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
  'Accept-Language': 'jp',
}

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'yodobashi.middlewares.YodobashiSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'yodobashi.middlewares.YodobashiDownloaderMiddleware': 543,
#}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
#ITEM_PIPELINES = {
#    'yodobashi.pipelines.YodobashiPipeline': 300,
#}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
# AUTOTHROTTLEに関連する設定は、Scrapyがリクエストの送信速度を自動的に調整するためのものです。
# これにより、対象サイトへの負荷を軽減し、クローリングの効率を最適化できます。


# 自動スロットリング機能を有効にするかどうか
#AUTOTHROTTLE_ENABLED = True
# AUTOTHROTTLE_ENABLED = True
# The initial download delay
# クローリング開始時に最初のリクエストを送るまでの初期遅延（秒単位）を設定
#AUTOTHROTTLE_START_DELAY = 5
# AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
# リクエスト間の最大遅延（秒単位）を設定
#AUTOTHROTTLE_MAX_DELAY = 60
# AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
# Scrapyが目指すリクエストの同時実行数
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
# 自動スロットリングのデバッグ情報を出力するかどうかを指定
# AUTOTHROTTLE_DEBUG = False #デフォルト
# AUTOTHROTTLE_DEBUG = True

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPキャッシュを使うか否か
# 2024/08/14保守でWebサーバーにアクセスできないので一旦キャッシュを切る
# HTTPCACHE_ENABLED = True
# HTTPCACHE_ENABLED = False
#HTTPキャッシュの保存期間(今回は1日で設定(秒))
# HTTPCACHE_EXPIRATION_SECS = 86400
#HTTPキャッシュの保存先
# HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

# ログレベルをDEBUGにする
LOG_LEVEL = 'DEBUG'
