#Dockerfile

FROM python:3.8

RUN mkdir -m 755 /var/src1

WORKDIR /var/src1

# 必要なパッケージのインストール
RUN apt-get update && \
    apt-get install -y \
    libxml2-dev \
    libxslt1-dev \
    libjpeg-dev \
    zlib1g-dev \
    libffi-dev \
    libssl-dev \
    # 必要なパッケージのインストール（wgetとgnupgを追加）
    wget \
    gnupg

# Google Chromeのインストール
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
    && apt-get update \
    && apt-get install -y google-chrome-stable

# # ChromeDriverのインストール
# RUN CHROME_DRIVER_VERSION=`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE` && \
#     wget -N http://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip -P ~/ && \
#     unzip ~/chromedriver_linux64.zip -d ~/ && \
#     rm ~/chromedriver_linux64.zip && \
#     mv -f ~/chromedriver /usr/local/bin/chromedriver && \
#     chown root:root /usr/local/bin/chromedriver && \
#     chmod 0755 /usr/local/bin/chromedriver

# ChromeDriverのインストール

# 1. Google Chromeのバージョンを取得
# 2. バージョンが115以上の場合:
#    - 新しいChrome for Testing用のURLからChromeDriverをダウンロード
#    - ダウンロードに失敗した場合はエラーメッセージを表示
# 3. ダウンロードしたChromeDriverが存在しない場合:
#    - 従来のURLからChromeDriverをダウンロード
# 4. ChromeDriverを/usr/local/bin/にインストール
# 5. 不要なzipファイルを削除
# 6. ChromeDriverの所有者とパーミッションを設定

RUN CHROME_VERSION=$(google-chrome --version | awk '{ print $3 }' | awk -F'.' '{ print $1 }') \
    && if [ "${CHROME_VERSION}" -ge "115" ]; then \
         CHROMEDRIVER_VERSION=$(curl -s "https://googlechromelabs.github.io/chrome-for-testing/LATEST_RELEASE_${CHROME_VERSION}") \
         && wget -N https://storage.googleapis.com/chrome-for-testing-public/${CHROMEDRIVER_VERSION}/linux64/chromedriver-linux64.zip -P ~/ || true \
         && if [ -f ~/chromedriver-linux64.zip ]; then \
              unzip ~/chromedriver-linux64.zip -d ~/ \
              && mv -f ~/chromedriver-linux64/chromedriver /usr/local/bin/chromedriver \
              && rm -rf ~/chromedriver-linux64 ~/chromedriver-linux64.zip; \
            else \
              echo "Failed to download ChromeDriver. Falling back to previous version."; \
            fi; \
       fi \
    && if [ ! -f /usr/local/bin/chromedriver ]; then \
         CHROMEDRIVER_VERSION=$(curl -s "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_${CHROME_VERSION}") \
         && wget -N https://chromedriver.storage.googleapis.com/${CHROMEDRIVER_VERSION}/chromedriver_linux64.zip -P ~/ \
         && unzip ~/chromedriver_linux64.zip -d ~/ \
         && mv -f ~/chromedriver /usr/local/bin/chromedriver \
         && rm ~/chromedriver_linux64.zip; \
       fi \
    && chown root:root /usr/local/bin/chromedriver \
    && chmod 0755 /usr/local/bin/chromedriver

RUN pip install ipython==7.22.0
RUN pip install pylint==2.7.2
RUN pip install autopep8==1.5.6
RUN pip install scrapy==2.4.1
RUN pip install selenium==3.141.0
RUN pip install scrapy-selenium==0.0.7
RUN pip install pymongo==3.11.3
RUN pip install pillow==8.2.0
RUN pip install dnspython==1.16.0
RUN pip install pyopenssl==22.0.0
RUN pip install cryptography==38.0.1
RUN pip install parsel==1.7.0
RUN pip install urllib3==1.26.15
RUN pip install twisted==22.10.0
RUN pip install shub
# pytest関係のライブラリー
RUN pip install pytest pytest-cov pytest-xdist
# MySQLドライバーをインストール
RUN pip install scrapy mysql-connector-python
# PostgreSQLドライバーをインストール
RUN pip install psycopg2
# .envからユーザー情報を呼び出せるようにインストール
RUN pip install python-dotenv
# ボリュームと同じ
COPY ./ /var/src1
#scrapy-selenium dnspython shub

CMD /bin/bash 
