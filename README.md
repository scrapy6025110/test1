# Scrapyを利用したWebスクレイピングプロジェクト  

> Pythonのフレームワーク「Scrapy」を使用した効率的なWebクローリングとデータ抽出  

***  
<br>

## 目次
1. [プロジェクトの概要](#プロジェクトの概要)
   - [目的](#目的)
   - [背景](#背景)
2. [環境設定](#環境設定)
3. [インストール方法](#インストール方法)
4. [使用方法](#使用方法)
5. [実装内容](#実装内容)
6. [ファイル構造](#ファイル構造)
7. [注意事項](#注意事項)
8. [ライセンス](#ライセンス)
9. [貢献方法](#貢献方法)
10. [参考資料](#参考資料)

***
<br>

## ○プロジェクトの概要　　
###  (目的)  
・  Python,Docker,GitLab CI/CD,pytestの技術の更なる向上、Scrapyの技術の獲得。  

・  Scrapyの技術の習得を通して、クローラー、スクレーピング、requests,BeautifulSoup,seleniumの知識の向上。  
・  現場レベルでのクローラー、スクレーピングの技術をScrapyを通して習得する。  
・  クローラーの安全な運用方法の取得。  
###  (背景)  
・  チャットbot、画像認証の際、データを手動で集めるのがとても大変だったので。  
・  生成AIのお陰でスクレーピングの需要が伸びている為。    
・  異なる言語での開発において、Pythonとの差異とスキルの転用を考察しつつ、Pythonの特性を活かした開発能力の維持。

***
<br>  

## ○環境設定  
＊  Dockerコンテナのバージョンがこのようになっているのは教材での指定＊  
###  (ローカルPC)  
・  OS:  macOS Big Sur 11.7.10  
・  Python:  3.9.7  
・  Scrapy:  2.11.0  
・  scrapy-selenium:  0.0.7  
・  selenium:  4.5.0  
###  (Dockerコンテナ)
・  OS:  Linux version 5.15.49-linuxkit-pr (root@buildkitsandbox) (gcc (Alpine 10.2.1_pre1) 10.2.1 20201203, GNU ld (GNU Binutils) 2.35.2)  
・  Python:  3.8.19  
・  Scrapy:  2.4.1  
・  scrapy-selenium:  0.0.7  
・  selenium:  3.141.0
<br>

###  ```Dockerfile::スパイダーが入っているコンテナ用```  
＊  各種パッケージ、ライプラリー、Chromeとchromedriverを入れる為のDockerfile  ＊  
<br>
```Dockerfile```


    #Dockerfile

    FROM python:3.8

    RUN mkdir -m 755 /var/src1

    WORKDIR /var/src1

    # 必要なパッケージのインストール
    RUN apt-get update && \
        apt-get install -y \
        libxml2-dev \
        libxslt1-dev \
        libjpeg-dev \
        zlib1g-dev \
        libffi-dev \
        libssl-dev \
        # 必要なパッケージのインストール（wgetとgnupgを追加）
        wget \
        gnupg

    # Google Chromeのインストール
    RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
        && echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
        && apt-get update \
        && apt-get install -y google-chrome-stable

    # # ChromeDriverのインストール
    # RUN CHROME_DRIVER_VERSION=`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE` && \
    #     wget -N http://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip -P ~/ && \
    #     unzip ~/chromedriver_linux64.zip -d ~/ && \
    #     rm ~/chromedriver_linux64.zip && \
    #     mv -f ~/chromedriver /usr/local/bin/chromedriver && \
    #     chown root:root /usr/local/bin/chromedriver && \
    #     chmod 0755 /usr/local/bin/chromedriver

    # ChromeDriverのインストール

    # 1. Google Chromeのバージョンを取得
    # 2. バージョンが115以上の場合:
    #    - 新しいChrome for Testing用のURLからChromeDriverをダウンロード
    #    - ダウンロードに失敗した場合はエラーメッセージを表示
    # 3. ダウンロードしたChromeDriverが存在しない場合:
    #    - 従来のURLからChromeDriverをダウンロード
    # 4. ChromeDriverを/usr/local/bin/にインストール
    # 5. 不要なzipファイルを削除
    # 6. ChromeDriverの所有者とパーミッションを設定

    RUN CHROME_VERSION=$(google-chrome --version | awk '{ print $3 }' | awk -F'.' '{ print $1 }') \
        && if [ "${CHROME_VERSION}" -ge "115" ]; then \
            CHROMEDRIVER_VERSION=$(curl -s "https://googlechromelabs.github.io/chrome-for-testing/LATEST_RELEASE_${CHROME_VERSION}") \
            && wget -N https://storage.googleapis.com/chrome-for-testing-public/${CHROMEDRIVER_VERSION}/linux64/chromedriver-linux64.zip -P ~/ || true \
            && if [ -f ~/chromedriver-linux64.zip ]; then \
                unzip ~/chromedriver-linux64.zip -d ~/ \
                && mv -f ~/chromedriver-linux64/chromedriver /usr/local/bin/chromedriver \
                && rm -rf ~/chromedriver-linux64 ~/chromedriver-linux64.zip; \
                else \
                echo "Failed to download ChromeDriver. Falling back to previous version."; \
                fi; \
        fi \
        && if [ ! -f /usr/local/bin/chromedriver ]; then \
            CHROMEDRIVER_VERSION=$(curl -s "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_${CHROME_VERSION}") \
            && wget -N https://chromedriver.storage.googleapis.com/${CHROMEDRIVER_VERSION}/chromedriver_linux64.zip -P ~/ \
            && unzip ~/chromedriver_linux64.zip -d ~/ \
            && mv -f ~/chromedriver /usr/local/bin/chromedriver \
            && rm ~/chromedriver_linux64.zip; \
        fi \
        && chown root:root /usr/local/bin/chromedriver \
        && chmod 0755 /usr/local/bin/chromedriver

    RUN pip install ipython==7.22.0
    RUN pip install pylint==2.7.2
    RUN pip install autopep8==1.5.6
    RUN pip install scrapy==2.4.1
    RUN pip install selenium==3.141.0
    RUN pip install scrapy-selenium==0.0.7
    RUN pip install pymongo==3.11.3
    RUN pip install pillow==8.2.0
    RUN pip install dnspython==1.16.0
    RUN pip install pyopenssl==22.0.0
    RUN pip install cryptography==38.0.1
    RUN pip install parsel==1.7.0
    RUN pip install urllib3==1.26.15
    RUN pip install twisted==22.10.0
    RUN pip install shub
    # MySQLドライバーをインストール
    RUN pip install scrapy mysql-connector-python
    # PostgreSQLドライバーをインストール
    RUN pip install psycopg2
    # .envからユーザー情報を呼び出せるようにインストール
    RUN pip install python-dotenv
    # ボリュームと同じ
    COPY ./ /var/src1
    #scrapy-selenium dnspython shub

    CMD /bin/bash 
<br>  

###  ```Dockerfile::PostgreSQL用のコンテナ用```  
＊PostgreSQLのロケール対策用に作成したDockerfile＊  
<br>
```Dockerfile```

    # PostgreSQLのロケール対策でオリジナルのDockerfileを用意
    # ベースイメージとしてPostgreSQLを使用
    FROM postgres:latest

    # ロケールパッケージをインストールし、ja_JP.UTF-8ロケールを生成
    RUN apt-get update && apt-get install -y locales \
        && localedef -i ja_JP -f UTF-8 ja_JP.UTF-8 \
        && apt-get clean

    # 環境変数を設定
    ENV LANG ja_JP.UTF-8
    ENV LC_ALL ja_JP.UTF-8

    # デフォルトのPostgreSQL初期化スクリプトディレクトリに、ロケール設定を反映させる
    COPY initdb-postgres.sh /docker-entrypoint-initdb.d/

    # initdb-postgres.shを作成
<br>

###  ```docker-compose.yml```
＊dosukoi-container,mongodb,mysql,postgreコンテナを立ち上げるdocker-compose.yml＊  
<br>
!!!パスワードが入っていますが、テスト環境用なのでこのままで表示します。本番環境の場合は.envファイルで呼び出せるようにして下さい!!!  
<br>
```docker-compose.yml```


    version: '3.8'

    services:
    scrapy:
        image: test1scrapy # Dockerイメージ
        build: . # 現在のディレクトリをビルドコンテキストとして指定
        container_name: dosukoi-container # コンテナ名
        # コンテナが起動後終了するので以下の3つを追加
        stdin_open: true
        tty: true
        restart: always
        depends_on: # 各DBに頼られている
        - mongodb
        - mysql
        - postgres
        environment:
        # 各接続URI。ポート番号の次はデータベース名
        MONGODB_URI: mongodb://mongodb:27017
        MYSQL_URI: mysql://root:example@mysql:3306/mysql_scrapydb
        POSTGRES_URI: postgres://postgres:example@postgres:5432/postgres_scrapydb
        volumes:
        - ./:/var/src1
        # コンテナが起動後終了するので試しにこれをコメントアウト
        # command: bash -c "cd /var/src1"
        # command: /bin/bash

    mongodb:
        image: mongo:latest
        container_name: mongodb
        ports:
        - "27017:27017"
        volumes:
        - mongodb_data:/data/db

    mysql:
        image: mysql:latest
        container_name: mysql
        environment:
        MYSQL_ROOT_PASSWORD: example
        MYSQL_DATABASE: mysql_scrapydb
        # データをUTF-8する
        MYSQL_CHARSET: utf8mb4
        ports:
        - "3306:3306"
        volumes:
        - mysql_data:/var/lib/mysql
        # ローカルPCのdocker-compose.ymlファイルと並列に
        # my.cnfファイルを作成。MySQLに設定を書き込む
        - ./my.cnf:/etc/mysql/conf.d/my.cnf

    postgres:
        # Dockerfileの指定
        build: ./postgres_dockerfile_dir/
        # Dockerfileを別途用意したのでコメントアウト
        # image: postgres:latest
        container_name: postgres
        environment:
        POSTGRES_PASSWORD: example
        POSTGRES_DB: postgres_scrapydb
        # データをUTF-8する
        # POSTGRES_INITDB_ARGS: "--encoding=UTF8"
        POSTGRES_INITDB_ARGS: "--encoding=UTF-8 --locale=ja_JP.UTF-8"
        # タイムゾーンの設定
        TZ: Asia/Tokyo
        ports:
        - "5432:5432"
        volumes:
        - postgres_data:/var/lib/postgresql/data
        # ローカルPCのDockerファイルと並列にpostgres-confディレクトリ作成
        # postgres-confディレクトリ配下にpostgres.confファイルを作成して
        # PostgreSQLの設定ファイル/etc/postgresql/conf.d/postgres.conf
        # に上書き
        - ./postgres-conf:/etc/postgresql/conf.d
        # - ./postgres-conf:/etc/postgresql/conf.d/postgresql.conf:/etc/postgresql/postgresql.conf
        # postgresユーザーがutf8にならないのでコンテナ内の~/.bash_profileでutf8を明示する
        - ./postgres-config/.bash_profile:/var/lib/postgresql/.bash_profile


    volumes: # 各ボリュームを記述
    mongodb_data:
    mysql_data:
    postgres_data:

<br>  

***  

<br>

## ○インストール方法
GitHub/GitLabのアカウントの取得,Docker-desktopが入っているとします。   
- ディレクトリの作成とgit初期化  

```bash```  

    ・mkdir -m 755 test1  
    ・cd test1  
    ・git init  

<br>

- GitLabのプロジェクトページにアクセスします。プロジェクト名はscrapy/test1です。  
<br>
- Codeからclone with sshかclone with httpsを選択してコピーして下さい。  
<br>
- 選択したコピーを貼り付けてクローンします。  

```bash```  

    ・git clone git@gitlab.com:グループ名/プロジェクト名.git (SSH)

     または、 

    ・git clone https://gitlab.com/グループ名/プロジェクト名.git  (HTTPS)  
<br>

- クローンが成功したら、Dockerイメージを作成します。  
  
```bash```  

    ・docker image build -t test1scrapy:latest .
<br>

- Dockerコンテナを立ち上げます。

```bash```  

    ・docker-compose up -d  
<br>

- Dockerコンテナ(スパイダーが入ったdosukoi-container)に入ります。

```bash```  

    ・docker exec -it dosukoi-container /bin/bash  
<br>

これで、インストールとスパイダーを使う準備はできました。  
<br>

***  

<br>

## ○使用方法  

・ここでの目次  
1. 新規のスパイダーを作成・起動させる場合  
2. 既存のスパイダーを動作させる場合  
3. 簡単なsettings.pyの設定方法

<br>
＊＊＊  1.新規のスパイダーを作成・起動させる場合  ＊＊＊

<br>
カレントディレクトリ確認。/var/src1にします。

```bash```  

    ・pwd
    ・cd /var/src1

<br>
プロジェクトディレクトリを作成して、プロジェクトディレクトリに移動、雛形のスパイダー、settings.py等を作成します。

```bash```  

    ・scrapy startproject プロジェクト名
    ・cd プロジェクトディレクトリ
    ・scrapy genspider スパイダー名 URL

＊ URLの部分はプロトコル(https://など)と最後に'/'がついている場合は削除して下さい。  

<br>

＊＊＊  2.既存のスパイダーを動作させる場合  ＊＊＊  

<br>

各プロジェクトディレクトリに移動してscrapyコマンドを打ちます(/var/src1からの移動)。  
<br>
コマンドラインでオプション'-o'をつけてjson,csv,xmlファイルを出力することができます。  

```bash```  

    例）JSONファイルを出力

    ・scrapy crawl スパイダー名 -o data.json

<br>

```*注意*```  

スパイダーが走っているのにも関わらず、データが表示されない場合は、XPath,CSSセレクターが変更されたかURLパスが変更された可能性があるのでその際はチェックをして下さい。  
<br>
  
- Qiitaのその日の記事のトレンドを表示する  

```bash```  

    ・cd test1scrapy
    ・scrapy crawl qiita_trend_1d

- ヨドバシカメラからデスクトップパソコンの在庫確認をする  

```bash```  

    ・cd yodobashi
    ・scrapy crawl desktop

- 紀伊国屋書店からコンピューター関連の書籍の一覧を取得してSQLite3,MongoDB,MySQL,PostgreSQLへ格納する  

```bash```  

    ・cd kinokuniya
    ・scrapy crawl computer_books

- Bing際でPythonの記事一覧をScrapy-Seleniumを使って取得

```bash```  

    ・cd bing
    ・scrapy crawl bing_python

- Antennaサイトで無限スクロールするサイトをScrapy-Seleniumを使って要素(タイトルとURL)を取得(実際はスクロールしないがコードはこのような感じ)

```bash```  

    ・cd antenna
    ・scrapy crawl antenna_tokushima

- 上のAntennaサイトの無限スクロール対策をseleniumでスクロールして要素(タイトルとURL)を取得

```bash```  

    ・cd unit_crawler
    ・python antenna_tokushima.py

- scrapyのFormRequestを使ってログインする

```bash```  

    ・cd test_login
    ・scrapy crawl login

- Scrapy-Seleniumを使ってログインする

```bash```  

    ・cd scrapy_selenium_login
    ・scrapy crawl sc_sele_login

<br>
＊＊＊  3.簡単なsettings.pyの設定方法  ＊＊＊ 

<br>
ここでは簡単なsettings.pyの設定方法を記述しておきます。settings.pyの設定は各プロジェクトで適切に設定して下さい。

<br>

```settings.py```  

    # プロジェクト名
    BOT_NAME = 'myproject'

    # Spider modules
    SPIDER_MODULES = ['myproject.spiders']
    NEWSPIDER_MODULE = 'myproject.spiders'

    #文字コードの追加
    FEED_EXPORT_ENCODING = 'utf-8'

    # クローリングの礼儀として、User-agentを設定
    USER_AGENT = 'MyProject Bot (+http://www.yourdomain.com)'

    # Webサーバーにrobots.txtがある際にはそれに従うか(基本True)
    ROBOTSTXT_OBEY = True

    # 同じドメインへのリクエスト間隔（秒）
    DOWNLOAD_DELAY = 3

    # ------以下は必要に応じて------

    #言語設定を日本語にする
    DEFAULT_REQUEST_HEADERS = {
        "Accept-Language": "ja",
    }

    # HTTPキャッシュを使うか否か
    HTTPCACHE_ENABLED = True

    # HTTPキャッシュの保存期間(今回は1日で設定(秒))
    HTTPCACHE_EXPIRATION_SECS = 86400

    # HTTPキャッシュの保存先
    HTTPCACHE_DIR = 'httpcache'

    # ログレベルの設定
    LOG_LEVEL = 'INFO'

    # 項目パイプラインの有効化
    ITEM_PIPELINES = {
    'myproject.pipelines.MyProjectPipeline': 300,
    }

<br>

***  

<br>

## ○実装内容  


### (基本編)  
1.    Qiitaの人気記事のスクレーピング  
        プロジェクト名:  test1scrapy  
        説明:  初めてのScrapyを使ったクローラー。Qiitaのその日の記事のトレンドを表示する。また後にユニットテストをここで実行します。

2.  ヨドバシカメラからデスクトップパソコンのメーカー、ブランド、価格を複数のページから取得  
        プロジェクト名:  yodobashi  
        説明:  要素を取得して、ページをクロールする  

3.  crawlテンプレートを使った紀伊国屋書店でのスパイダーの作成  
        プロジェクト名:  kinokuniya  
        説明:  crawlテンプレートを使ったクローラー。また後にDB接続ここで行います。

### (応用編)  
4.  pytestの実装  
        パス:  /var/src1/test1scrapy/test1scrapy/tests/test_qiita_1d.py  
        説明:  ユニットテストが書かれています。

5.  gitlab-ci.ymlでのランナーの立ち上げ  
        パス:  /var/src1/.gitlab-ci.yml  
        説明:  GitLab CI/CDを使います。PUSHからMERGE間でユニットテスト、総合テストを行います。  

6.  DBへの接続(Dockerfile,docker-compose.ymlの設定とsettings.py)  
        プロジェクト名:  kinokuniya  
        説明:  MongoDB,MySQL,PostgreSQLへの接続と要素の格納をします。また、PostgreSQLではDBの設定、ロケールの設定も行います。  

7.    CSVファイルからPostgreSQLへの格納  
        プロジェクト名:  kinokuniya  
        説明:  CSVファイルからPostgreSQLへデータの格納を行います。  

8.    スタティックにPostgreSQLからCSVファイルを出力  
        プロジェクト名:  kinokuniya  
        説明:  コマンドラインからのCSVファイルの出力でなく、必要な要素だけを出力するCSVファイルを作成します。  

9.    紀伊国屋書店のサイトから画像を取得してディレクトリに格納する  
        プロジェクト名:  kinokuniya  
        説明:  本の画像を取得します。  

10.   BingサイトでのScrapy-Seleniumの利用(基本編)  
        プロジェクト名:  bing  
        説明:  Scrapy-Seleniuを使って要素の一覧を取得します。  

11.   AntennaサイトでのDocker+Scrapy-Seleniumを使った画面スクロール後の要素の取得  
        プロジェクト名:  antenna  
        説明:  Docker+Scrapy-Seleniumを使った無限スクロールの画面から要素を取得します。  

12.   AntennaサイトでのDocker+seleniumを使った画面スクロール後の要素の取得  
        パス:  /var/src1/unit_crawler/antenna_tokushima.py  
        説明:  Docker+Scrapy-Seleniumでの画面スクロールがうまくいかなかった場合の代替案。  

13.   .envファイルの利用  
        パス:  /var/src1/.env  
        説明:  これまではユーザー情報などは直接書いていたがこれ以降は.envファイルから呼び出すようにする。  
        追加説明:  本来、.envファイルは.gitignoreに記述しますが、今回は学習用なのでそのまま表示しますが、本番環境では必ず.gitignoreに記述して下さい。  

14.   FormReqest.from_responseを使ったログイン画面の突破  
        プロジェクト名:  test_login  
        説明:  Scrapyを使ったログイン画面の攻略  

15.   Scrapy-Seleniumを使ったログイン画面の突破  
        プロジェクト名:  Scrapy_Selenium_login  
        説明:  Scrapy-Seleniumを使ったログイン画面の攻略  



<br> 

***  
<br>

## ○ファイル構造  
/var/src1からのtreeを表示します。    

<br>  

```/var/src1/から```  

    .
    ├── Dockerfile
    ├── README.md
    ├── .DS_Store
    ├── .coverage
    ├── .env
    ├── .git
    ├── .gitignore
    ├── .gitlab-ci.yml
    ├── .pytest_cache
    ├── antenna
    │   ├── antenna
    │   │   ├── __init__.py
    │   │   ├── __pycache__
    │   │   │   ├── __init__.cpython-38.pyc
    │   │   │   ├── __init__.cpython-39.pyc
    │   │   │   ├── settings.cpython-38.pyc
    │   │   │   └── settings.cpython-39.pyc
    │   │   ├── items.py
    │   │   ├── middlewares.py
    │   │   ├── pipelines.py
    │   │   ├── settings.py
    │   │   └── spiders
    │   │       ├── __init__.py
    │   │       ├── __pycache__
    │   │       │   ├── __init__.cpython-38.pyc
    │   │       │   ├── __init__.cpython-39.pyc
    │   │       │   ├── antenna_tokushima.cpython-38.pyc
    │   │       │   └── antenna_tokushima.cpython-39.pyc
    │   │       └── antenna_tokushima.py
    │   ├── compare_search_result_tokushima2.png
    │   ├── data.json
    │   ├── fillout_tokushima.png
    │   ├── first_top_page.png
    │   ├── scrapy.cfg
    │   └── search_result_tokushima.png
    ├── bing
    │   ├── 01_open_bing.png
    │   ├── 02_after_fill_out_bing.png
    │   ├── 03_results_python_bing.png
    │   ├── 04_second_results_python_bing.png
    │   ├── 6page_result_shoots.png
    │   ├── bing
    │   │   ├── __init__.py
    │   │   ├── __pycache__
    │   │   │   ├── __init__.cpython-38.pyc
    │   │   │   ├── __init__.cpython-39.pyc
    │   │   │   ├── settings.cpython-38.pyc
    │   │   │   └── settings.cpython-39.pyc
    │   │   ├── items.py
    │   │   ├── middlewares.py
    │   │   ├── pipelines.py
    │   │   ├── settings.py
    │   │   └── spiders
    │   │       ├── __init__.py
    │   │       ├── __pycache__
    │   │       │   ├── __init__.cpython-38.pyc
    │   │       │   ├── __init__.cpython-39.pyc
    │   │       │   ├── bing_python.cpython-38.pyc
    │   │       │   └── bing_python.cpython-39.pyc
    │   │       ├── bing_python.py
    │   │       └── chromedriver
    │   ├── chromedriver
    │   ├── current_0719.json
    │   ├── data.json
    │   └── scrapy.cfg
    ├── books_toscrape
    │   ├── 20240418205941_log_test.txt
    │   ├── 20240719104026_log_test.txt
    │   ├── 20240719104259_log_test.txt
    │   ├── __pycache__
    │   │   └── test_debug.cpython-39-pytest-8.2.0.pyc
    │   ├── after.csv
    │   ├── after.json
    │   ├── before.json
    │   ├── books_toscrape
    │   │   ├── __init__.py
    │   │   ├── __pycache__
    │   │   │   ├── __init__.cpython-38.pyc
    │   │   │   ├── __init__.cpython-39.pyc
    │   │   │   └── settings.cpython-38.pyc
    │   │   ├── items.py
    │   │   ├── middlewares.py
    │   │   ├── pipelines.py
    │   │   ├── settings.py
    │   │   └── spiders
    │   │       ├── __init__.py
    │   │       ├── __pycache__
    │   │       │   ├── __init__.cpython-38.pyc
    │   │       │   ├── __init__.cpython-39.pyc
    │   │       │   ├── books_crawl.cpython-38.pyc
    │   │       │   └── books_crawl.cpython-39.pyc
    │   │       └── books_crawl.py
    │   ├── current_0719.json
    │   ├── scrapy.cfg
    │   └── test_debug.py
    ├── docker-compose.yml
    ├── kinokuniya
    │   ├── addskip_insertcsv_to_mysql.py
    │   ├── after_test.json
    │   ├── before_test.json
    │   ├── book_images
    │   │   └── computer_books
    │   │       └── 取得したpngファイル多数  
    │   ├── booksdata.json
    │   ├── current_0719.json
    │   ├── from_get_mysql.csv
    │   ├── get_mysql.py
    │   ├── get_postgresql.csv
    │   ├── get_postgresql.py
    │   ├── get_sqlite3.py
    │   ├── insertcsv_to_mysql.py
    │   ├── insertcsv_to_postgresql.py
    │   ├── kinokuniya
    │   │   ├── __init__.py
    │   │   ├── __pycache__
    │   │   │   ├── __init__.cpython-38.pyc
    │   │   │   ├── items.cpython-38.pyc
    │   │   │   ├── pipelines.cpython-38.pyc
    │   │   │   └── settings.cpython-38.pyc
    │   │   ├── get_mongodb.py
    │   │   ├── items.py
    │   │   ├── middlewares.py
    │   │   ├── pipelines.py
    │   │   ├── settings.py
    │   │   └── spiders
    │   │       ├── __init__.py
    │   │       ├── __pycache__
    │   │       │   ├── __init__.cpython-38.pyc
    │   │       │   └── computer_books.cpython-38.pyc
    │   │       └── computer_books.py
    │   ├── open.db
    │   ├── scrapy.cfg
    │   ├── tmp.csv
    │   └── tmp.json
    ├── memo.yaml
    ├── my.cnf
    ├── postgres-conf
    │   └── postgres.conf
    ├── postgres-config
    ├── postgres_dockerfile_dir
    │   ├── Dockerfile
    │   └── initdb-postgres.sh
    ├── scrapy_selenium_login
    │   ├── comform_changing_page.png
    │   ├── fill_in_form.png
    │   ├── scrapy.cfg
    │   └── scrapy_selenium_login
    │       ├── __init__.py
    │       ├── __pycache__
    │       │   ├── __init__.cpython-38.pyc
    │       │   └── settings.cpython-38.pyc
    │       ├── items.py
    │       ├── middlewares.py
    │       ├── pipelines.py
    │       ├── settings.py
    │       └── spiders
    │           ├── __init__.py
    │           ├── __pycache__
    │           │   ├── __init__.cpython-38.pyc
    │           │   └── sc_sele_login.cpython-38.pyc
    │           └── sc_sele_login.py
    ├── test1scrapy
    │   ├── read_json.py
    │   ├── rm_data_json.sh
    │   ├── scrapy.cfg
    │   └── test1scrapy
    │       ├── __init__.py
    │       ├── __pycache__
    │       │   ├── __init__.cpython-38.pyc
    │       │   ├── __init__.cpython-39.pyc
    │       │   ├── settings.cpython-38.pyc
    │       │   └── settings.cpython-39.pyc
    │       ├── items.py
    │       ├── middlewares.py
    │       ├── pipelines.py
    │       ├── settings.py
    │       ├── spiders
    │       │   ├── __init__.py
    │       │   ├── __pycache__
    │       │   │   ├── __init__.cpython-38.pyc
    │       │   │   ├── __init__.cpython-39.pyc
    │       │   │   ├── example.cpython-38.pyc
    │       │   │   ├── qiita_trend_1d.cpython-38-pytest-8.2.0.pyc
    │       │   │   ├── qiita_trend_1d.cpython-38.pyc
    │       │   │   └── qiita_trend_1d.cpython-39.pyc
    │       │   └── qiita_trend_1d.py
    │       └── tests
    │           ├── __init__.py
    │           ├── __pycache__
    │           │   ├── __init__.cpython-38.pyc
    │           │   ├── __init__.cpython-39.pyc
    │           │   ├── test_qiita_trend_1d.cpython-38-pytest-8.2.0.pyc
    │           │   └── test_qiita_trend_1d.cpython-39-pytest-8.2.0.pyc
    │           └── test_qiita_trend_1d.py
    ├── test_login
    │   ├── scrapy.cfg
    │   └── test_login
    │       ├── __init__.py
    │       ├── __pycache__
    │       │   ├── __init__.cpython-38.pyc
    │       │   └── settings.cpython-38.pyc
    │       ├── items.py
    │       ├── middlewares.py
    │       ├── pipelines.py
    │       ├── settings.py
    │       └── spiders
    │           ├── __init__.py
    │           ├── __pycache__
    │           │   ├── __init__.cpython-38.pyc
    │           │   └── login.cpython-38.pyc
    │           └── login.py
    ├── unit_crawler
    │   ├── after_screenshot.png
    │   ├── antena_top_page.png
    │   ├── antenna_tokushima.py
    │   ├── first_screenshot.png
    │   ├── first_try_selenium_in_docker.py
    │   ├── full_down_tokushima_first_page.png
    │   ├── result.csv
    │   ├── result.json
    │   ├── tokushima_first_page.png
    │   └── what_do_fill_in_search_window.png
    └── yodobashi
        ├── desktop_2024-03-26T11-57-25.csv
        ├── scrapy.cfg
        └── yodobashi
            ├── __init__.py
            ├── __pycache__
            │   ├── __init__.cpython-38.pyc
            │   └── settings.cpython-38.pyc
            ├── items.py
            ├── middlewares.py
            ├── pipelines.py
            ├── settings.py
            └── spiders
                ├── __init__.py
                ├── __pycache__
                │   ├── __init__.cpython-38.pyc
                │   └── desktop.cpython-38.pyc
                └── desktop.py  



<br> 

***  
<br>

## ○注意事項
## 注意事項

1. スクレイピングの倫理と法的遵守
   - サイトの利用規約を遵守する（スクレイピングが明示的に禁止されていないか確認）
   - 個人情報や著作権のあるコンテンツの取り扱いに注意する
   - 必要以上のデータを収集しない
   - 法的・倫理的な観点からのコンプライアンスを確保する

2. サーバーへの配慮
   - サーバーに過度な負荷をかけない
   - リクエストの頻度を適切に制限する（例：DOWNLOAD_DELAYの設定）
   - 並列処理の数を制限する
   - キャッシュを活用し、不必要なリクエストを避ける

3. robots.txtの遵守
   - ROBOTSTXT_OBEYをTrueに設定
   - robots.txtで許可されていない部分はスクレイピングしない

4. 適切な設定と対応
   - ユーザーエージェントを適切に設定する（自身のボットであることを明確にする）
   - エラー処理を適切に行い、サイトの変更に対応できるようにする
   - サイトの構造が変更された場合の対応策を準備する

5. 効率的なデータ収集
   - 必要なデータのみを収集し、不要なリクエストを避ける
   - データの保存と管理を適切に行う

<br>

***  
<br>

## ○ライセンス(License)

このプロジェクトはMITライセンスの下でライセンスされています。詳細については[LICENSE](LICENSE)ファイルを参照してください。  
<br>

---English---  
<br>

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.  

<br>

***  
<br>

## ○貢献方法

このプロジェクトへの貢献を歓迎します。以下のガイドラインを参照してください。

- 貢献の種類

  - バグの報告と修正
  - 新機能の提案と実装
  - ドキュメントの改善
  - コードのリファクタリング  
<br>

- プロセス

  1. GitLabアカウントをお持ちでない場合は、作成してください。
  2. このリポジトリをフォークします。
  3. フォークしたリポジトリをローカルPCにクローンします(インストール方法を参照して下さい)。  
  4. クローンしたディレクトリに移動します。  
        ```bash```
        ```    
        cd リポジトリ名
        ```
  5. 新しいブランチを作成します。  
        ```bash```
        ```
        git checkout -b feature/AmazingFeature
        ```
  6. 変更を加え、変更をステージングします。  
        ```bash```
        ```
        git add .
        ```
  7. 変更をコミットします。  
        ```bash```
        ```
        git commit -m 'Add some AmazingFeature'
        ```
  8. ブランチにプッシュします。  
        ```bash```
        ```
        git push origin feature/AmazingFeature
        ```
  9.  GitHubで、フォークしたリポジトリからオリジナルのリポジトリに対してプルリクエストを作成します。  
<br>

- コーディング規約

  - [PEP 8](https://www.python.org/dev/peps/pep-0008/)に従ってコードを書いてください。
  - コメントは日本語で記述してください。
  - 関数やクラスには適切なドキュメンテーション文字列を付けてください。  
<br>

- イシューの報告  
バグを見つけた場合や新機能を提案したい場合は、GitLabのIssueを使用してください。  

  - バグ報告の場合は、再現手順を詳細に記述してください。  
  - 新機能の提案の場合は、その機能がどのように役立つかを説明してください。  
<br>

- プルリクエスト

  1. プルリクエストは既存のイシューに関連付けてください。
  2. 大きな変更の場合は、事前にイシューで議論することをお勧めします。
  3. プルリクエストには、変更内容の簡潔な説明を含めてください。  
<br>

- コミュニケーション  
質問や議論がある場合は、GitLabのIssueを使用してください。  
<br>

## ご協力ありがとうございます！

<br>

***  
<br>

## ○参考資料  
1. 教材
   - [題名/PythonでWebスクレイピング・クローリングを極めよう！（Scrapy・Selenium 編）], [著者/清水義孝], [最終更新日:2024/5], [URL/https://www.udemy.com/course/python-web-scraping-with-scrapy/?couponCode=JPLETSLEARNNOW]

2. ウェブサイト
   - [目的:ScrapyのItemLoaderの組み込みプロセッサー], [サイト名:Scrapy], URL:https://doc-ja-scrapy.readthedocs.io/ja/latest/topics/loaders.html#topics-loaders-available-processors , (アクセス日:2024.05)  
   - [目的:PostgreSQLのコマンド一覧(簡単版)], [サイト名:Qiita], URL:https://qiita.com/H-A-L/items/fe8cb0e0ee0041ff3ceb  , (アクセス日:2024.06)  
   - [目的:PostgreSQLのコマンド一覧(詳細版)], [サイト名:Qiita], URL:https://qiita.com/n4a/items/12169101624d854e661c  , (アクセス日:2024.06)
   - [目的:無限スクロール画面対策(基本版)], [サイト名:スケ郎のお話], URL:https://www.sukerou.com/2021/09/pythonselenium.html?m=1#google_vignette  , (アクセス日:2024.07) 
   - [目的:無限スクロール画面対策(発展版)], [サイト名:Qiita], URL:https://qiita.com/thistle0420/items/c80fb7b88896212493f5  , (アクセス日:2024.07)  
   - [目的:], [サイト名:], URL: , (アクセス日:) 

3. AI支援ツール
   - Anthropic社 Claude3
   - OpenAI社 ChatGPT
   - Google社 Gemini

本プロジェクトは上記の資料を参考に作成されました。各資料の著作権は、それぞれの著者・組織に帰属します。 

<br>

***

# --- English ---  
<br>

# This project employs Scrapy for Web scraping .  

> Efficient Web scraping and data extraction using Python's Scrapy framework .  

***  
<br>

## Content
1. [Project_summary](#Project_summary)  
   - [Purpose](#Purpose)
   - [Background](#Background)
2. [Environment_setup](#Environment_setup)
3. [Install](#Install)
4. [Using](#Using)
5. [Implementation_details](#Implementation_details)
6. [File-structure](#File-structure)
7. [Cautions](#Cautions)
8. [License](#License)
9. [Contribution_Guidelines](#Contribution_Guidelines)
10. [Reference](#Referencd)

***
<br>

## ○Project_summary    

###  (Purpose)  
・  To further enhance skills in Python, Docker, GitLab CI/CD, and pytest, and to acquire proficiency in Scrapy.

・  It will improve my skills in crawling, scraping, requests, BeautifulSoup, and Selenium through learning Scrapy.  

・  To gain hands-on experience in web crawling and scraping with Scrapy.  

・  It will learn safe practices for operating web crawlers.   

###  (Background)  
・  I found it very challenging to develop a chatbot and image authentication due to manual data gather process.  

・  The demand for scraping technology is being increased by generative AI.  

・  While developing in different languages, keeping Python development skills while considering differences from Python and how to transfer skills.  

***
<br>  

## ○環境設定  

ここから




　　


