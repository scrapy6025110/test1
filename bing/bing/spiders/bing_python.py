import scrapy,time
# scrapy-seleniumではリクエストをSeleniumRequestで取得する
# 単体のrequestsライブラリーみたいな物
from scrapy_selenium import SeleniumRequest

# 画像などの取得の為の時間制御
# webDriverWait(driverに待ち時間を設定)
from selenium.webdriver.support.ui import WebDriverWait
# webDriverWaitにどこまで待つかを指定する際に使用
from selenium.webdriver.support import expected_conditions as EC

# WebDriverのインスタンスに直接find_element_by_idなどのメソッドは使用出来ない
# なので、find_elementで指定していくのに必要なByをインポート
from selenium.webdriver.common.by import By  # 必要なインポート文

# キーの特殊操作をするためにインポート
from selenium.webdriver.common.keys import Keys

class BingPythonSpider(scrapy.Spider):
    name = 'bing_python'
    # allowed_domainsはscrapy-seleniumでは無効
    # allowed_domains = ['www.bing.com/?cc=jp']
    # start_requestsをオーバーライドするので不要
    # start_urls = ['http://www.bing.com/?cc=jp/']

    '''scrapyのリクエストをオーバーライドする'''
    def start_requests(self):
         yield SeleniumRequest(
             # start_urlsにsを追記
             url='https://www.bing.com/?cc=jp',
             # 3秒待つ(教材ではwaitを使っていたがサポートされていない)
             # もしどうしても時間を待たせたいなら
             # timeかWebDriverWaitを使います(他のメソッド部分で)。
            #  wait=3,
             # リクエストをparse関数に返す
             callback=self.parse
         )

    '''教材のコード'''
    # def parse(self, response):
    #     driver = response.meta['driver']
    #     # スクリーンショットを取得
    #     driver.save_screenshot('01_open_bing.png')
    
    '''メインの処理関数'''
    def parse(self, response):
        # responseのmeta情報(辞書型データ)にはdriverが含まれているか否かで判定
        if 'driver' not in response.meta:
            self.logger.error("Driver not found in response meta. SeleniumRequest may not be working correctly.")
            print('driverは見つかりません!!')
            return

        # driverの設定(単独のseleniumより簡単)
        driver = response.meta['driver']
        
        # WebDriverWaitを使用して、ページの読み込みが完了するまで最大10秒待つ
        wait = WebDriverWait(driver, 10)
        # bodyタグの要素を取得するまで待つ(結構使い勝手が良い)
        # wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body')))
        # 上だとhtmlのbody要素は取得するまで待ちますが、jaは待たないので最後のjaの読み取りをするまで待つようにしました。
        wait.until(EC.presence_of_all_elements_located((By.XPATH, "//script[@type='text/javascript'][52]")))
        
        # wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body')))の際は上手くいかなかったので一時的に使用
        # import time
        # time.sleep(10)
        
        print('driverが有りました。01_open_bing.pngを作成します。')
        # スクリーンショットの取得
        driver.save_screenshot('01_open_bing.png')
        self.logger.info(f"Page title: {driver.title}")
        
        '''------検索窓にpythonと入力してスクリーンショットを取得------'''
        
        # WebDriverのインスタンスに直接find_element_by_idなどのメソッドは
        # 使用出来ないなので、find_elementで指定していく
        # formタグはimputやtext,textareaの全体を囲うタグで実際の入力はimputなど
        # なので今回のXPathは//textarea[@id='sb_form_q']でした
        search_bar = driver.find_element(By.ID,'sb_form_q') 
        
        # driverのsend_keysメソッドを使ってpythonと入力
        search_bar.send_keys('python')
        
        # スクリーンショットの取得
        driver.save_screenshot('02_after_fill_out_bing.png')
        
        # 5秒間待つ
        # wait = WebDriverWait(driver, 5)
        # wait.until(EC.presence_of_all_elements_located((By.XPATH, "//script[@type='text/javascript'][52]")))
        
        # 上を関数化
        def few_wait(driver_name,wait_time,xpath):
            wait = WebDriverWait(driver_name, wait_time)
            # wait.until(EC.presence_of_all_elements_located((By.XPATH, xpath)))
            wait.until(EC.presence_of_element_located((By.XPATH, xpath)))
            # コンテナのseleniumのバージョンでは使えない
            # wait.until(EC.any_of(*conditions))
        
        # 5秒間待つ    
        few_wait(driver,5,"//script[@type='text/javascript'][52]")
        
        '''------検索窓にpythonと入力してスクリーンショットを取得------'''
        
        '''エンターボタンを押して検索画面に移動してからスクリーンショットを取得'''
        
        # エンターボタンを押す。
        search_bar.send_keys(Keys.ENTER)
        
        # 画面が遷移したことをターミナルで表示
        print('pythonの1ページ目の検索結果画面に遷移しました!')
        
        # スタティックに5秒待つ
        time.sleep(5)
        
        # 遷移後のURLを調べる
        current_url = driver.current_url
        print(f'現在のURLは// {current_url} //です')
        
        
        # 画面が遷移したので再定義
        driver = response.meta['driver']
        
        # 5秒間待つ    
        # few_wait(driver,5,"//script[164]") # エラー
        few_wait(driver,15,"//script[80]") # OK
        # 仮でtimeを使う
        # time.sleep(5)
        
        # 関数few_waitをオーバーライド
        # def few_wait(driver_name,wait_time,tag):
        #     # bodyタグの要素を取得するまで待つ(結構使い勝手が良い)
        #     wait.until(EC.presence_of_element_located((By.CSS_SELECTOR,tag)))
            
        # few_wait(driver,10,'body')
        
        # スクリーンショットの取得
        # そのままスクリーンショットを取得すると一部しか取れないのでjsを実行して
        # 高さと幅を取得
        w = driver.execute_script('return document.body.scrollWidth')
        h = driver.execute_script('return document.body.scrollHeight')
        # driverに高さと幅を定義
        driver.set_window_size(w,h)
        driver.save_screenshot('03_results_python_bing.png')
        
        '''エンターボタンを押して検索画面に移動してからスクリーンショットを取得'''
        
        ''' ---1ページ目の広告以外の検索一覧を表示----------------------'''
        
        '''
        タイトルのXPath: //li[@class='b_algo']/h2/a/text()
        URLのXPath: //li[@class='b_algo']/h2/a[@href]
        '''
        print('============================================')
        # オブジェクトとして//li[@class='b_algo']/h2/aまでを取得
        objects = driver.find_elements(By.XPATH,"//li[@class='b_algo']/h2/a")
        
        # オブジェクトをfor文で回す
        for i in objects:
            # テキストを取得
            title = i.text
            # href属性を取得
            url = i.get_attribute('href')
            # scrapy crawl bing_python -o ***.jsonファイルに出力するためにコーディング
            yield{
                'title': title,
                'URL': url,
            }
            print('********1ページ目のタイトルとURL*************')
        
        '''ターミナルに出力するためにコーディング'''
        # title_lists = []
        # url_lists = []
        # # print(objects)
        # for i in objects:
        #     # print(f'タイトルは{i.text}')
        #     # print(f'URLは{i.get_attribute("href")}')
        #     # print('--------------------------')
        #     title_lists.append(i.text)
        #     url_lists.append(i.get_attribute('href'))
            
        # print('============================================')
        
        # for j in title_lists:
        #     print(f'タイトルは{j}です')
        #     print('***********************')
            
        # print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
        
        # for k in url_lists:
        #     print(f'URLは{k}です')
        #     print('***********************')
        
        ''' ---1ページ目の広告以外の検索一覧を表示----------------------'''

        # ===2ページから5ページ目のスクリーンショットを取得=================
        
        '''次へのボタンのXPath:: //a[@title='次のページ'] '''
        
        # 次へのボタンのXPathを指定
        next_btn = driver.find_element(By.XPATH,"//a[@title='次のページ']")
        
        # 次へのボタンをエンターする(2ページ目に移行)
        # next_btn.send_keys(Keys.ENTER)
        
        # クリック
        next_btn.click()
        
        # スタティックに5秒待つ
        time.sleep(10)
        
        # スナップショットを取得
        driver.save_screenshot('04_second_results_python_bing.png')
        
        # 2ページ目に移行を表示
        print('検索結果2ページ目です!')
        
        '''2ページ目に移動'''
        
        ''''2ページ目のhtmlファイルのコンテンツを取得してオブジェクトを取得して典型する'''
        
        # page_sourceメソッドで現在のページのhtmlのコンテンツを取得
        # page_sourceはプロパティなのでstr型で返されるのでこの後要素をこのままでは取り出せない
        # page_contents = driver.page_source 
        
        # 取得したhtmlコンテントからオブジェクトを作成
        # objects = page_contents.find_elements(By.XPATH,"//li[@class='b_algo']/h2/a")
        
        objects = driver.find_elements(By.XPATH,"//li[@class='b_algo']/h2/a")
        
        # for文で回してobjectsからタイトルとURLを取得
        for i in objects:
            # テキストを取得
            title = i.text
            # href属性を取得
            url = i.get_attribute('href')
            # scrapy crawl bing_python -o ***.jsonファイルに出力するためにコーディング
            yield{
                'title': title,
                'URL': url,
            }
            print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
        
        
        print('2ページ目の検索結果からタイトルとURLを取得しました')
        
        # 次へのボタンのXPathを指定
        next_btn = driver.find_element(By.XPATH,"//a[@title='次のページ']")
        
        # 次へのボタンをエンターする(3ページ目に移行)
        # next_btn.send_keys(Keys.ENTER)
        
        # クリック
        next_btn.click()
        
        # スタティックに5秒待つ
        time.sleep(10)
        
        ''''2ページ目のhtmlファイルのコンテンツを取得してオブジェクトを取得して典型する'''
        
        '''----------------3~5ページ目のタイトルとURLを取得する(中小規模用)--------------'''
        '''関数化するコードをまとめて関数化'''
        # def rounding(self,number,driver):
        #     round_count = number
        #     while True:
        #         # XPathで要素(塊)を取得
        #         objects = driver.find_elements(By.XPATH,"//li[@class='b_algo']/h2/a")
                
        #         # for文で回してobjectsからタイトルとURLを取得
        #         for i in objects:
        #             # テキストを取得
        #             title = i.text
        #             # href属性を取得
        #             url = i.get_attribute('href')
        #             # scrapy crawl bing_python -o ***.jsonファイルに出力するためにコーディング
        #             yield{
        #                 'title': title,
        #                 'URL': url,
        #             }
        #             print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
                
        #         print(f'{round_count}ページ目の検索結果からタイトルとURLを取得しました')
                
        #         round_count += 1
                
        #         if round_count > 5:
        #             break
                
        #         # 次へのボタンのXPathを指定
        #         next_btn = driver.find_element(By.XPATH,"//a[@title='次のページ']")
                
        #         # クリック
        #         next_btn.click()
                
        #         # スタティックに5秒待つ
        #         time.sleep(10)
        
        # rounding(3)
        
        '''上の関数roundingをクラス直下に配置してジェネレータ関数として呼び出す'''
        
        yield from self.rounding(3,driver)
        
        '''----------------3~5ページ目のタイトルとURLを取得する(中小規模用)--------------'''
        
        '''================6~10ページ目のタイトルとURLを取得する(大規模用)============='''
        
        # yield SeleniumRequestを使ってurlとcallbackを指定callback関数で処理を記述
        # ＊＊＊yield Seleniumを使うなら上のyield from self.rounding(3,driver)は使わない＊＊＊
        
        # yield SeleniumRequest(
        #     # current_urlプロパティで現在のURLを取得(5ページ目)
        #     url = driver.current_url,
        #     callback = self.six_ten_page_parse,
        #     # 重複を禁止する
        #     dont_filter=True,
        # )
        
        
        # yield SeleniumRequestだと途中でセッションが切れるのでこちらで対応
        yield from self.six_ten_page_parse(response)
        
        '''================6~10ページ目のタイトルとURLを取得する(大規模用)============='''
        
        '''----------------3~5ページ目のスクショ---------------------------'''
        
        # # スクリーンショットの関数化
        # def three_to_five_shoots(number):
            
        #     # 次へのボタンのXPathを指定
        #     next_btn = driver.find_element(By.XPATH,"//a[@title='次のページ']")
            
        #     # クリック
        #     next_btn.click()
            
        #     # スタティックに5秒待つ
        #     time.sleep(10)
        
        #     # スナップショットを取得
        #     driver.save_screenshot(f'{number}pages_result_shoots.png')
        
        # for i in range(3,6):
        #     three_to_five_shoots(i)
        #     print('¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥')
        #     print(f'{i}ページ目です!')
        #     print('¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥')
            
        '''----------------3~5ページ目のスクショ---------------------------'''
       
    
    '''====parseメソッド内でジェネレーター関数で呼び出す(検索結果3~5ページ目)===='''
    # ここでのdriverは空だが、parseの中でdriverを定義しているのでfind_elementsが動く
    def rounding(self,number,driver):
        round_count = number
        while True:
            # XPathで要素(塊)を取得
            objects = driver.find_elements(By.XPATH,"//li[@class='b_algo']/h2/a")
                
            # for文で回してobjectsからタイトルとURLを取得
            for i in objects:
                # テキストを取得
                title = i.text
                # href属性を取得
                url = i.get_attribute('href')
                # scrapy crawl bing_python -o ***.jsonファイルに出力するためにコーディング
                yield{
                    'title': title,
                    'URL': url,
                }
                    
                print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
                
            print(f'{round_count}ページ目の検索結果からタイトルとURLを取得しました')
                
            round_count += 1
                
            if round_count > 5:
                break
                
            # 次へのボタンのXPathを指定
            next_btn = driver.find_element(By.XPATH,"//a[@title='次のページ']")
                
            # クリック
            next_btn.click()
                
            # スタティックに5秒待つ
            time.sleep(10)
            
    '''====parseメソッド内でジェネレーター関数で呼び出す(検索結果3~5ページ目)===='''
        
    '''-parse内でyield SeleniumRequestのcallback関数(6~10ページ目の検索結果)-'''
    
    #　##yield SeleniumRequestを使うと諸事情でうまくいかないのでyield fromで対応## #
    def six_ten_page_parse(self,response):
        # responseのmeta情報(辞書型データ)にはdriverが含まれているか否かで判定
        if 'driver' not in response.meta:
            self.logger.error("Driver not found in response meta. SeleniumRequest may not be working correctly.")
            print('driverは見つかりません!!')
            return
        
        # 現在のページ(5ページ目)のメタ情報からdriverを再設定
        driver = response.meta['driver']
        
        # -現在のページ(5ページ目)から6ページ目に移行-
        # 次へのボタンのXPathを指定
        next_btn = driver.find_element(By.XPATH,"//a[@title='次のページ']")
        
        # 検証用にスクショを取る
        driver.save_screenshot('6page_result_shoots.png')
                
        # クリック
        next_btn.click()
                
        # スタティックに5秒待つ
        time.sleep(10)
        
        # -6ページ目から10ページ目までのタイトルとURLを取得-
        round_count = 6
        while True:
        # for j in range(6,11):
            # XPathで要素(塊)を取得
            objects = driver.find_elements(By.XPATH,"//li[@class='b_algo']/h2/a")
                
            # for文で回してobjectsからタイトルとURLを取得
            for i in objects:
                # テキストを取得
                title = i.text
                # href属性を取得
                url = i.get_attribute('href')
                # scrapy crawl bing_python -o ***.jsonファイルに出力するためにコーディング
                yield{
                    'title': title,
                    'URL': url,
                }
                    
                print('#########6~10ページ目のタイトルとURL############')
                
            print(f'{round_count}ページ目の検索結果からタイトルとURLを取得しました')
            # print(f'{j}ページ目の検索結果からタイトルとURLを取得しました')
                
            round_count += 1
                
            if round_count > 10:
                break
            
            # if j == 10:
            #     break
                
            # 次へのボタンのXPathを指定
            next_btn = driver.find_element(By.XPATH,"//a[@title='次のページ']")
                
            # クリック
            next_btn.click()
                
            # スタティックに5秒待つ
            time.sleep(10)
    
    '''-parse内でyield SeleniumRequestのcallback関数(6~10ページ目の検索結果)-'''
