import scrapy
# scrapyでPOSTメソッドが可能となるライブラリーをインストール
from scrapy import FormRequest

# .envファイルからユーザー情報を取得する為のインポート
import os
from dotenv import load_dotenv


class LoginSpider(scrapy.Spider):
    # スパイダー名
    name = 'login'
    # /loginを削除
    allowed_domains = ['quotes.toscrape.com']
    # httpsにして最後の/を削除
    start_urls = ['https://quotes.toscrape.com/login']

    '''メインで処理する関数'''
    def parse(self, response):
        # csrf_tokenのXPath  //input[@name="csrf_token"]//@value
        
        # csrf_tokenを取得
        token = response.xpath('//input[@name="csrf_token"]//@value').get()
        
        # CSRF_TOKEN検証用
        print('--------------------------------')
        print(f'CSRF_TOKENは {token} ')
        print('--------------------------------')
        
        # .envファイルからのユーザー情報の取得
        # .envファイルからのユーザー情報の取得の為load_dotenvメソッドを呼び出す
        load_dotenv()
        # 検証用
        # print('--------------------------------')
        # print(os.getenv('USERNAME'))
        # print(os.getenv('PASSWORD'))
        # print('--------------------------------')
        
        # FormRequestからfrom_responseメソッドを使ってPOST送信する
        yield FormRequest.from_response(
            response,
            # formのXPathを指定
            formxpath='//form',
            # 送信するcsrf_token,username,passwordを指定
            formdata={
                # csrf_tokenを渡さないとエラーとなる
                'csrf_token': token,
                # エラー検証用
                # 'csrf_token': '',
                # .envファイルからユーザー情報を呼び出す
                'username': os.getenv('USERNAME'),
                'password': os.getenv('PASSWORD'),
            },
            # コールバック関数を指定
            callback=self.after_login
        )
    
    '''ログイン後の処理をするメソッド
       ログインに成功するとログイン画面でlogoutボタンが現れる
       のでそこのテキストでログイン出来たかできなかったかを判別
    '''    
    def after_login(self,response):
        # logoutのXPath  //a[@href="/logout"]/text()
        
        if response.xpath('//a[@href="/logout"]/text()').get():
            print('--------------------------------')
            print('ログインに成功しました')
            print('--------------------------------')
        else:
            print('--------------------------------')
            print('ログインに失敗しました')
            print('--------------------------------')
        
    
