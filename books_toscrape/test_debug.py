#VScodeでのデバックを行うためのファイル

#デフォルト
import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
process = CrawlerProcess(settings=get_project_settings())

#プロジェクトディレクトリからのパスbooks_toscrape/spiders/books_crawl.pyから
#BooksCrawlSpiderクラスを呼び出している
from books_toscrape.spiders.books_crawl import BooksCrawlSpider

#クラスBooksCrawlSpiderを呼び出してその中のスパイダーを使用する
process.crawl(BooksCrawlSpider)
#デバックをスタートする
process.start()
