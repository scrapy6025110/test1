import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
#一応
import logging
#Scrapy shellをスパイダー実行時に立ち上げたい場合
from scrapy.shell import inspect_response



class BooksCrawlSpider(CrawlSpider):
    #スパイダー名
    name = 'books_crawl'
    #ドメイン
    allowed_domains = ['books.toscrape.com']
    #始めるURL
    start_urls = ['https://books.toscrape.com/catalogue/category/books/fantasy_19/page-1.html']

    rules = (
        #詳細ページへのリンク(20個)
        Rule(LinkExtractor(restrict_xpaths='//h3/a'), callback='parse_item', follow=False),
        #nextへのリンク(1個)
        Rule(LinkExtractor(restrict_xpaths='//li[@class="next"]/a'), follow=True),
    )
    
    
    #データ整形用の関数
    #価格をint型にする
    def get_price(self,price):
        if price:
            price = float(price.lstrip('£'))
            return price
        return price #ブレークポイント指定
    
    #在庫状況の改行コードを削除 
    def get_stock(self,stock):
        if stock:
            stock = stock.replace('\n    \n        ','').replace('\n    \n','')
            return stock
        return stock
    
    #評価数をint型にする
    def get_review_count(self,review_count):
        if review_count:
            review_count = int(review_count)
            return review_count
        return review_count

    #詳細データを取得する関数
    def parse_item(self, response):
        
        #ログの設定(フレームワークを使わない場合はこの様に書く)
        #今回のScrapyの場合はsettings.pyに記述する
        
        # logging.basicConfig(
        #     #filename='ファイル名を指定', #ログを書き込むファイル名
        #     filename=str(datetime.datetime.now())+'_log_test.txt', #ログを書き込むファイル名
        #     level=logging.DEBUG, #ログのレベルを指定
        #     format='%(asctime)s - %(levelname)s - %(message)s' #ログのフォーマット(時刻、ログレベル、メッセージ)
        # )
        
        #ログをターミナルに出力する場合はprintで見やすくする
        #ログをファイルに出力する場合はprintを切る
        # print('---------ログスタート----------')
        
        #ログの出力。urlを返す
        # logging.info(response.url)
        # logging.debug(response.status)
        
        #辞書型でも可能
        logging.debug({
            'URL': response.url,
            'STATUS': response.status,
            'PRICE': self.get_price(response.xpath('//div[@class="col-sm-6 product_main"]/p[@class="price_color"]/text()').get()),
        })
        
        # print('---------ログエンド----------')
        
        #Scrapy shellを実行したいところで起動するこちらのコード
        #selfはこのスパイダー自身を指しています
        inspect_response(response,self)
        
        yield{
            'title': response.xpath('//h1/text()').get(),
            'price': self.get_price(response.xpath('//div[@class="col-sm-6 product_main"]/p[@class="price_color"]/text()').get()),
            'stock': self.get_stock(response.xpath('//div[@class="col-sm-6 product_main"]/p[@class="instock availability"]/text()[2]').get()),
            'review': response.xpath('//div[@class="col-sm-6 product_main"]/p[3]/@class').get(),
            'upc': response.xpath('//tr[1]/td/text()').get(),
            'review_count': self.get_review_count(response.xpath('//tr[7]/td/text()').get()),
        }
