# PostgreSQLからCSVファイルにデータを出力する

# ライブラリーのインストール
import os,csv,psycopg2

# try~except~finallyで例外処理を追加する為にpsycopg2からErrorをインポート
from psycopg2 import Error

# データベースの接続情報

host = os.getenv('POSTGRES_HOST', 'postgres')
database = os.getenv('POSTGRES_DATABASE', 'postgres_scrapydb')
# PostgreSQLではデフォルトでrootユーザーは存在しないのでデフォルトユーザーのpostgresする
user = os.getenv('POSTGRES_USER', 'postgres')
password = os.getenv('POSTGRES_PASSWORD', 'example')

# try~except~finallyでハンドリング正常処理、例外処理、最終処理を定義
try:
    # データベースの接続 
    db = psycopg2.connect(
        host=host,
                database=database,
                user=user,
                password=password,
                # 文字化け対策でUTF8を指定
                client_encoding='UTF8',
                # 更にオプションでclient_encodingでutf8を指定
                # options='-c client_encoding=UTF8'
    )

    # カーソルの作成
    c = db.cursor()

    # PostgreSQLからデータの取得してリスト化
    get_sql = 'select * from computer_books;'
    c.execute(get_sql)
    get_sql_lists = []
    get_sql_lists = c.fetchall()

    # CSVファイルの指定と展開
    
    # file_pathは名前を定義したことに過ぎません
    file_path = 'get_postgresql.csv'
    
    # os.path.isfile()メソッドを使ってopen()メソッド後にファイルの有無で条件分岐する
    file_exists = os.path.isfile(file_path) #True/False
    
    # modeは敢えてaにした
    # ここのopen()メソッドで実際にファイルが作成されます
    with open(file_path,mode='a',newline='',encoding='utf8') as f:
        weiter = csv.writer(f)
        
        # もし、file_pathが無いならヘッダーを追加,既存であるならヘッダーは記述しない
        if not file_exists: #not FalseでTrueとなるなら実行
            header = ['title','author','price','publisher','size','page','isbn']
            weiter.writerow(header)
        
        for i in get_sql_lists:
            weiter.writerow(i)

    print(f'{file_path}ファイルを作成しました')
    print(f'初期CSVファイルの有無: {file_exists}')
except (Exception,Error) as e:
    print(f'エラーが発生しました: {e}')
finally:
    # リソースの解放
    c.close
    db.commit()
    db.close()

