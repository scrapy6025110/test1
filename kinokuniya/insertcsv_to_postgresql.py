# CSVファイルをUTF-8にエンコーディングしてデータをPostgreSQLに格納する
import os,csv,psycopg2

# データベースの作成・接続.カーソルの作成

host = os.getenv('POSTGRES_HOST', 'postgres')
database = os.getenv('POSTGRES_DATABASE', 'postgres_scrapydb')
# PostgreSQLではデフォルトでrootユーザーは存在しないのでデフォルトユーザーのpostgresする
user = os.getenv('POSTGRES_USER', 'postgres')
password = os.getenv('POSTGRES_PASSWORD', 'example')

db = psycopg2.connect(
    host=host,
            database=database,
            user=user,
            password=password,
            # 文字化け対策でUTF8を指定
            client_encoding='UTF8',
            # 更にオプションでclient_encodingでutf8を指定
            # options='-c client_encoding=UTF8'
)

c = db.cursor()

# テーブルの作成(無ければ)

create_table_if_exists_sql = '''
            CREATE TABLE IF NOT EXISTS computer_books (
                title VARCHAR(1024),
                author VARCHAR(1024),
                price INTEGER,
                publisher VARCHAR(1024),
                size VARCHAR(1024),
                page INTEGER,
                isbn VARCHAR(1024) PRIMARY KEY
            );
        '''

c.execute(create_table_if_exists_sql)

# CSVファイルの指定

file_path = 'tmp.csv' # スパイダーで出力したSCVファイル

# CSVファイルの展開(エンコーディング等)

with open(file_path,mode='r',newline='',encoding='utf8') as f:
    reader = csv.reader(f)
    # ヘッダーを削除
    next(reader)
    
    '''
    # (検証用)
    # readerはリストを格納したオブジェクトで、readerの中のリストは
    # ['ISBN', 'author', 'page', 'price', 'publisher', 'size', 'title']
    # で並んでいる
    
    # print(reader) # readerのタイプ確認
    for i in reader:
        # CSVの並びをPostgreSQLのテーブルの並びに帰る
        print(f'タイトルは{i[6]}、著者は{i[1]}、価格は{i[3]}、出版社は{i[4]}、サイズは{i[5]}、ページは{i[2]}、ISBNは{i[0]}です')
        for j in range(0,7): # 0~6まで
            print(j)
            print(type(i[j])) # 全てのデータがstr型
    '''
    
    # テーブルcomputer_booksにデータを格納する
    insert_data_sql = 'insert into computer_books values(%s,%s,%s,%s,%s,%s,%s);'
    try:
        for i in reader:
            # isdigit()メソッドは変数が整数か否かを判断する
            # a = 123 a.isdigit() # True
            # b = '123' b.isdigit() # True
            # c = 1.23 c.isdigit() # False
            i[3] = int(i[3]) if i[3].isdigit() else None
            i[2] = int(i[2]) if i[2].isdigit() else None
            c.execute(
                insert_data_sql,
                # (i[6],i[1],int(i[3]),i[4],i[5],int(i[2]),i[0])
                (i[6],i[1],i[3],i[4],i[5],i[2],i[0])
                
            )
    except psycopg2.IntegrityError:
            # 重複キーの場合はスキップ
            db.rollback()
    except Exception as e:
            db.rollback()
            raise e
    
    print('tmp.csvファイルからデータを格納しました。')
    
db.commit()
db.close()
        