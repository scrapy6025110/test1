#MongoDBからデータを取得するファイル


import pymongo
from settings import MONGODB_URI

#---MongoDBへの接続-------------------------

# settings.pyのMONGODB_URIを取得
# uri = spider.settings.get('MONGODB_URI')

# open_spiderメソッドは使わないのでこちらで対応
uri = MONGODB_URI

# MongoDBに接続
client = pymongo.MongoClient(uri)
# データベース名を指定(選択) show dbsから指定も可
db = client[uri.split('/')[-1]]

# (新規)コレクションを指定(選択) show collectionsから
collections = db['scrapy_items']

#---MongoDBへの接続-------------------------

for i in collections.find():
    print(i)

# これを利用してCSVやJSONなどに出力することも可能
