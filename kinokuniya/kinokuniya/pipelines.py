# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

#ある値が取得できない場合にそのデータを削除(Drop)するクラスをインポート
from scrapy.exceptions import DropItem

#MongoDBのドライバーpymongoをインストール
import pymongo
#SQLite3をインポート
import sqlite3
# settings.pyから変数を呼び出せるようにする
import os
# MySQLドライバーのインポート
import mysql.connector
# PostgreSQLドライバーのインストール
import psycopg2
# 画像ファイルを取得するImagesPipelineクラスを呼び出す。
from scrapy.pipelines.images import ImagesPipeline


#例外処理発生の場合raiseするクラスデータを格納しない(Drop)
class CheckItemPipeline:
    def process_item(self, item, spider):
        if not item.get('ISBN'):
            raise DropItem('ISBNが取得できませんでした')
        return item

#MongoDBにデータを格納するクラス
class MongoDBPipeline:
    def open_spider(self, spider):
        # settings.pyのMONGODB_URIを取得
        uri = spider.settings.get('MONGODB_URI')
        #検証用
        # print('----------------------------------')
        # print(f"Connecting to MongoDB at {uri}")
        # print('----------------------------------')
        # MongoDBに接続
        self.client = pymongo.MongoClient(uri)
        #データベース名を指定(選択)
        self.db = self.client[uri.split('/')[-1]]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        #検証用
        # print('----------------------------------')
        # print(f"Inserting item into MongoDB: {item}")
        # print('----------------------------------')
        # settings.pyからMONGODB_COLLECTIONを取得
        collection_name = spider.settings.get('MONGODB_COLLECTION', 'scrapy_items')
        # コレクション名を指定して辞書型でデータを格納
        self.db[collection_name].insert_one(dict(item))
        return item

#SQLite3でのデータの格納
class SQLite3Pipeline:
    # SQLite3への接続
    def open_spider(self,spider):
        self.db_path = 'open.db'
        self.db = sqlite3.connect(self.db_path)
        self.c = self.db.cursor()
        self.sql1 = 'create table if not exists computer_books(title text,author text,price int,publisher text,size text,page int,isbn text primary key);'
        self.sql2 = 'insert into computer_books values(?,?,?,?,?,?,?);'
        # try-except文でテーブルが既にできていても処理を実行する
        try:
            self.c.execute(self.sql1)
            self.db.commit()
        except sqlite3.OperationalError:
            pass
    
    # データを格納する
    def process_item(self,item,spider):
        # リストの作成
        # self.lists = [item.get('title'),item.get('author'),item.get('price'),item.get('publisher'),item.get('size'),item.get('page'),item.get('ISBN')]
        # タプルの作成
        self.taples = (item.get('title'),item.get('author'),item.get('price'),item.get('publisher'),item.get('size'),item.get('page'),item.get('ISBN'))
        # self.c.executemany(self.sql2,self.lists)
        # 一度に複数のレコードを渡すのではないのでexecute(SQL文,タプル)
        self.c.execute(self.sql2,self.taples)
        self.db.commit()
        return item
    
    # データベースを閉じる
    def close_spider(self,spider):
        self.db.close()

#MySQLでのデータの格納
class MySQLPipeline:
    # データベースへの接続
    def open_spider(self, spider):
        # user = os.getenv('MYSQL_USER', 'user')
        # password = os.getenv('MYSQL_PASSWORD', 'abcdef')
        # host = os.getenv('MYSQL_HOST', 'localhost')
        # database = os.getenv('MYSQL_DATABASE', 'computer_books')
        user = os.getenv('MYSQL_USER', 'root')
        password = os.getenv('MYSQL_PASSWORD', 'example')
        host = os.getenv('MYSQL_HOST', 'mysql')
        database = os.getenv('MYSQL_DATABASE', 'mysql_scrapydb')
        
        self.db = mysql.connector.connect(
            user=user,
            password=password,
            host=host,
            database=database,
            # encoding
            charset='utf8mb4',
            # MySQLのバージョンによってはこれも必要
            # collation='utf8_general_ci'
            # collation='utf8mb4_unicode_ci'
            collation='utf8mb4_0900_ai_ci'
        )
        self.c = self.db.cursor()
        
        # self.sql1 = 'create table if not exists computer_books(title text,author text,price int,publisher text,size text,page int,isbn text primary key );'
        # MySQLでisbnをプライマリーキーにする場合は以下の方法で
        # self.sql1 = 'create table if not exists computer_books(title text,author text,price int,publisher text,size text,page int,isbn text, primary key (isbn(255)));'
        self.sql1 = 'create table if not exists computer_books(title VARCHAR(128) CHARACTER SET utf8mb4,author VARCHAR(128) CHARACTER SET utf8mb4,price int,publisher VARCHAR(128) CHARACTER SET utf8mb4,size VARCHAR(128) CHARACTER SET utf8mb4,page int,isbn VARCHAR(128) CHARACTER SET utf8mb4, primary key (isbn));'
        self.sql2 = 'set names utf8mb4;'
        self.c.execute(self.sql2)
        self.c.execute(self.sql1)

    # データベースの切断
    def close_spider(self, spider):
        self.db.commit()
        self.c.close()
        self.db.close()

    #データベースへの格納
    def process_item(self, item, spider):
        # placeholders = ', '.join(['%s'] * len(item))
        # columns = ', '.join(item.keys())
        # sql = f"INSERT INTO computer_books ({columns}) VALUES ({placeholders})"
        sql = 'insert into computer_books values(%s,%s,%s,%s,%s,%s,%s);'
        item_values = [item.get('title').encode('utf-8'),item.get('author').encode('utf-8'),item.get('price'),item.get('publisher').encode('utf-8'),item.get('size').encode('utf-8'),item.get('page'),item.get('ISBN').encode('utf-8')]
        # item_values = [str(item.get('title'), 'utf-8mb4'), str(item.get('author'), 'utf-8mb4'), item.get('price'), str(item.get('publisher'), 'utf-8mb4'), str(item.get('size'), 'utf-8mb4'), item.get('page'), str(item.get('ISBN'), 'utf-8mb4')]
        # item_values = [
        #     item.get('title', '').encode('utf-8', 'surrogateescape'), 
        #     item.get('author', '').encode('utf-8', 'surrogateescape'),
        #     item.get('price'), 
        #     item.get('publisher', '').encode('utf-8', 'surrogateescape'),
        #     item.get('size', '').encode('utf-8', 'surrogateescape'),
        #     item.get('page'),
        #     item.get('ISBN', '').encode('utf-8', 'surrogateescape')
        # ]
        # レコードの挿入
        try:
            self.c.execute(sql,item_values)
        except mysql.connector.IntegrityError as err:
            # 重複キーの場合はスキップ
            if err.errno == 1062:
                pass
            else:
                raise
        # Encode each field to UTF-8
        # item_values = [str(value).encode('utf-8') if isinstance(value, str) else value for value in item.values()]
        
        # self.c.execute(sql, item_values)
        
        # self.c.execute(sql, list(item.values()))
        self.db.commit()
        return item


# PostgreSQLへのデータの格納
class PostgreSQLPipeline:
    def open_spider(self, spider):
        host = os.getenv('POSTGRES_HOST', 'postgres')
        database = os.getenv('POSTGRES_DATABASE', 'postgres_scrapydb')
        # PostgreSQLではデフォルトでrootユーザーは存在しないのでデフォルトユーザーのpostgresする
        user = os.getenv('POSTGRES_USER', 'postgres')
        password = os.getenv('POSTGRES_PASSWORD', 'example')

        self.db = psycopg2.connect(
            host=host,
            database=database,
            user=user,
            password=password,
            # 文字化け対策でUTF8を指定
            client_encoding='UTF8',
            # 更にオプションでclient_encodingでutf8を指定
            # options='-c client_encoding=UTF8'
        )
        # self.db.autocommit = True  # オートコミットモードをTrueに
        self.c = self.db.cursor()

        self.create_table_query = '''
            CREATE TABLE IF NOT EXISTS computer_books (
                title VARCHAR(1024),
                author VARCHAR(1024),
                price INTEGER,
                publisher VARCHAR(1024),
                size VARCHAR(1024),
                page INTEGER,
                isbn VARCHAR(1024) PRIMARY KEY
            );
        '''
        self.c.execute(self.create_table_query)
        # self.db.autocommit = False # その後オートコミットモードをFalseに戻す

    def close_spider(self, spider):
        self.db.commit()
        self.c.close()
        self.db.close()

    def process_item(self, item, spider):
        # self.c.execute("SELECT to_regclass('public.computer_books')")
        # if not self.c.fetchone()[0]:
        #     # テーブルが存在しない場合はopen_spiderからクエリを実行
        #     self.open_spider(spider)
        insert_query = '''
            INSERT INTO computer_books (title, author, price, publisher, size, page, isbn)
            VALUES (%s, %s, %s, %s, %s, %s, %s);
        '''
        
        item_values = (
            # こちらの形だとコンテナのrootユーザーはOK。
            # コンテナpostgresユーザーだと文字化け
            
            item.get('title'),
            item.get('author'),
            item.get('price'),
            item.get('publisher'),
            item.get('size'),
            item.get('page'),
            item.get('ISBN')
            
            # itemをUTF-8にエンコーディングするencode('utf-8')
            # コンテナroot,postgresユーザー共に上とは違う形の文字化け
            
            # item.get('title').encode('utf-8'),
            # item.get('author').encode('utf-8'),
            # item.get('price'),
            # item.get('publisher').encode('utf-8'),
            # item.get('size').encode('utf-8'),
            # item.get('page'),
            # item.get('ISBN').encode('utf-8')
        )

        try:
            # self.db.autocommit = False # トランザクション開始
            self.c.execute(insert_query, item_values)
        except psycopg2.IntegrityError:
            # 重複キーの場合はスキップ
            self.db.rollback()
        except Exception as e:
            self.db.rollback()
            raise e
        # finally:
            # self.db.autocommit = True  # オート・コミット・モードに戻す

        # psycopg2にはcursorにはcommit()は有りません
        # self.c.commit()
        return item

# 画像ファイルを取得するImagePipelineクラスのfile_path関数
# をオーバーライドする
class CustomImagePipeline(ImagesPipeline):
    def file_path(self, request, response=None, info=None, *, item=None):
        # # ファイル名をアイテムからタイトル名を呼び出して.jpgを加える
        # name = item.get('title') + '.jpg'
        
        # 2024/08/03(土)に他のスパイダーを動作させたら画像ファイル名が
        # 長すぎるとCI/CDの方でエラーが出たので、画像ファイル名の文字数制限を追加
        
        name = item.get('title')
        # ファイル名を50文字に制限
        max_length = 50
        if len(name) > max_length:
            name = name[:max_length] # [:max_length] == [:50] 50まで
        
        name += '.jpg' # name = name + '.jpg'
        
        # settings.pyで記述した./book_images配下にcomputer_booksディレクトリを作成
        # してその配下に画像ファイルを格納する
        file_dir_file_name = 'computer_books/' + name
        return file_dir_file_name
    

   