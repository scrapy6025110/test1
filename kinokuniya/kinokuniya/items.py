# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


#---今回使うプロセッサーの説明---

#MapComposeは主にinput_processorで使いデータの整形に使う
#TaksFirst,Joinはリスト型のデータからstr型のデータにする際に使う
#TakeFirst,Joinは主にoutput_processorで使う

#---今回使うプロセッサーの説明---
#itemloaderのプロセッサーからTakeFirst,MapCompose,Joinを呼び出す
from itemloaders.processors import TakeFirst,MapCompose,Join

#---MapComposeで使う関数定義---

#円マークを削除する関数
def strip_yen(element):
    if element:
        return element.replace('¥','')
    return element

#カンマを削除する関数
def strip_conma(element):
    if element:
        return element.replace(',','')
    return element

#str型からint型に変換する関数
def convert_integer(element):
    if element:
        return int(element)
    return 0

#全角/を起点に要素1(インデックス0)を取得する関数
def split_var1(element):
    if element:
        return element.split('／')[0]
    return element

#全角/を起点に要素2(インデックス1)を取得する関数
def split_var2(element):
    if element:
        return element.split('／')[1]
    return element

#サイズの要素の余分な部分を取り除く関数
def rej_size(element):
    if element:
        return element.replace('サイズ ','')
    return element

#ページ数の要素から余分な部分を取り除く関数
def rej_page1(element):
    if element:
        return element.replace('ページ数 ','')
    return element

#ページ数の要素から余分な部分を取り除く関数
def rej_page2(element):
    if element:
        return element.replace('p' ,'')
    return element

#商品コードからいらない部分を削除する関数
def rej_goods_code(element):
    if element:
        return element.replace('商品コード ','')
    return element

#---MapComposeで使う関数定義---

class BookItem(scrapy.Item):
    title = scrapy.Field(
        #左の空白を削除
        input_processor = MapCompose(str.lstrip),
        #複数のタイトル要素を空白1つ入れて1つの文字列で繋ぐ
        output_processor = Join(' ')
    )
    author = scrapy.Field(
        #作者が２人以上いる場合は空白1つ入れて1つの文字列で繋ぐ
        output_processor = Join(' ')
    )
    publisher = scrapy.Field(
        #出版社の値をリスト型の要素から1番目の要素を取得
        output_processor = TakeFirst()
    )
    price = scrapy.Field(
        #input_processorでMapComposeを使い関数strip_yen,strip_conma,
        #convert_integerを順次使用する
        input_processor = MapCompose(strip_yen,strip_conma,convert_integer),
        #output_processorでリストの最初の要素を取得
        output_processor = TakeFirst()
    )
    size = scrapy.Field(
        input_processor = MapCompose(split_var1,rej_size),
        output_processor = TakeFirst()
    )
    page = scrapy.Field(
        input_processor = MapCompose(split_var2,rej_page1,rej_page2,convert_integer),
        output_processor = TakeFirst()
    )
    ISBN = scrapy.Field(
        input_processor = MapCompose(rej_goods_code),
        output_processor = TakeFirst()
    )
    # 画像ファイルのフィールドを追加
    image_urls = scrapy.Field()
