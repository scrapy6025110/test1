# Scrapy settings for kinokuniya project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

#docker-compose.ymlファイルの設定の環境設定を読み取る為にインポート
import os

#botの名前
BOT_NAME = 'kinokuniya'

SPIDER_MODULES = ['kinokuniya.spiders']
NEWSPIDER_MODULE = 'kinokuniya.spiders'

#文字コードの追加
FEED_EXPORT_ENCODING = 'utf-8'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#＊＊＊今回はUSER_AGENTは表記しない＊＊＊
#USER_AGENT = 'kinokuniya (+http://www.yourdomain.com)'

# Obey robots.txt rules
#サーバーにrobots.txtがあればそれに従うか否か
ROBOTSTXT_OBEY = True

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#クローラーを当てる間隔(秒)
DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#言語を日本語化
DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
  'Accept-Language': 'jp',
}

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'kinokuniya.middlewares.KinokuniyaSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'kinokuniya.middlewares.KinokuniyaDownloaderMiddleware': 543,
#}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html

#---pipelines.pyのクラスCheckItemPipelineを呼び出す---
#バリューは小さい方が優先される(0~1000)

ITEM_PIPELINES = {
   'kinokuniya.pipelines.CheckItemPipeline': 100,
   # 'kinokuniya.pipelines.MongoDBPipeline': 300,
   # 'kinokuniya.pipelines.SQLite3Pipeline': 300,
   # 'kinokuniya.pipelines.MySQLPipeline': 300,
   'kinokuniya.pipelines.PostgreSQLPipeline': 300,
   # 画像イメージをパイプラインに走らせる
   # これはコンテナの/usr/local/lib/python3.8/site-packages
   # /scrapy/pipelines/images.pyのImagesPipelineクラスを呼び出している
   # 'scrapy.pipelines.images.ImagesPipeline' : 400,
   # site-packages/scrapy/pipelines/images.pyのImagePipelineクラスの
   # 関数file_pathのオーバーライドした関数をkinokuniya/pipelines.pyの
   # customImagePipelineクラスを呼び出す。
   'kinokuniya.pipelines.CustomImagePipeline': 400,
}

#---pipelines.pyのクラスCheckItemPipelineを呼び出す---

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#キャッシュを使います
HTTPCACHE_ENABLED = True
#HTTPキャッシュの保存期間(今回は1日で設定(秒))
# HTTPCACHE_EXPIRATION_SECS = 86400
#不便なのでHTTPキャッシュを1週間にする
HTTPCACHE_EXPIRATION_SECS = 604800
#HTTPキャッシュの保存先
HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

#---データの取得制御---------------------

#リクエストの数を1とする(平行処理を解除)
CONCURRENT_REQUESTS = 1

#階層の深さにより、1の場合は深くなると優先度が下がる
#これにより人気順にリクエストを送信できる
DEPTH_PRIORITY = 1

#この2つでLIFOからFIFOに変更してリクエストのキューの順番を人気順にする
SCHEDULER_DISK_QUEUE = 'scrapy.squeues.PickleFifoDiskQueue'
SCHEDULER_MEMORY_QUEUE = 'scrapy.squeues.FifoMemoryQueue'

#---データの取得制御---------------------

#===データベースの処理===================

# ###各データベースとURIで接続###

#MongoDB
MONGODB_URI = os.getenv('MONGODB_URI','mongodb://localhost:27017')

#MySQL
MYSQL_URI = os.getenv('MYSQL_URI', 'mysql://user:abcdef@localhost:3306/computer_books')

# docker-compose.ymlファイルのURI設定より
# MYSQL_URI: mysql://root:example@mysql:3306/mysql_scrapydb
# ://ユーザー名:パスワード@ホスト名:ポート番号/データベース名


# ###各データベースとURIで接続###

# MongoDBのコレクション(SQLのテーブル)を指定
MONGODB_COLLECTION = 'scrapy_items'

# MySQLの情報
# MYSQL_USER = os.getenv('MYSQL_USER', 'user')
# MYSQL_PASSWORD = os.getenv('MYSQL_PASSWORD', 'abcdef')
# MYSQL_HOST = os.getenv('MYSQL_HOST', 'localhost')
# MYSQL_DATABASE = os.getenv('MYSQL_DATABASE', 'computer_books')

MYSQL_USER = os.getenv('MYSQL_USER', 'root')
MYSQL_PASSWORD = os.getenv('MYSQL_PASSWORD', 'example')
MYSQL_HOST = os.getenv('MYSQL_HOST', 'mysql')
MYSQL_DATABASE = os.getenv('MYSQL_DATABASE', 'mysql_scrapydb')

# PostgreSQLの情報
# PostgreSQLではデフォルトでrootユーザーは存在しないのでデフォルトユーザーのpostgresする
POSTGRES_USER = os.getenv('POSTGRES_USER', 'postgres')
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD', 'example')
POSTGRES_HOST = os.getenv('POSTGRES_HOST', 'postgres')
POSTGRES_DATABASE = os.getenv('POSTGRES_DATABASE', 'postgres_scrapydb')
#===データベースの処理===================

#---画像ファイル関係---------------------

# イメージファイルを格納するフォルダー
IMAGES_STORE = './book_images'

# 画像イメージのフィールドを指定(items.pyに記述してある)
IMAGE_URLS_FIELD = 'image_urls'

#---画像ファイル関係---------------------

