from cgi import print_exception
from dataclasses import replace
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
import logging

#items.pyからクラスBookItemをインポート
from kinokuniya.items import BookItem

#Item Loaderをインポート(itemを呼び出せる)
from scrapy.loader import ItemLoader


class ComputerBooksSpider(CrawlSpider):
    name = 'computer_books' #スパイダー名
    #ドメイン名
    allowed_domains = ['www.kinokuniya.co.jp']
    #クロールし始めるURL
    start_urls = ['https://www.kinokuniya.co.jp/f/dsd-101001037028005-06-']

    #crawlテンプレートから作成するとrules属性の中で、Ruleオブジェクトを作成する
    rules = (
        #XPathでの詳細ページの指定、LinkExtractorで指定したXPathを
        #辿らないようにするのでfollowはFalseとする(詳細ページに行かないようにする)
        Rule(LinkExtractor(restrict_xpaths='//h3[@class="heightLine-2"]/a'), callback='parse_item', follow=False),
        #CSSセレクタの指定
        # Rule(LinkExtractor(restrict_css="h3.heightLine-2 > a"), callback='parse_item', follow=False),
        
        #上のルールが終わったら次へのリンクを辿るルール
        #次へのリンクを辿るだけなのでcallback入らない。followもいならいが最初なので明示する
        Rule(LinkExtractor(restrict_xpaths='(//a[contains(text(),"次へ")])[1]'), follow=True),
        #CSSセレクタの場合
        # Rule(LinkExtractor(restrict_css='a:contains("次へ")'), follow=True),
    )
    
    #titleを整形する関数
    def get_title(self,title):
        if title:
            #joinメソッドでリストの中身は連結してstrに変更
            #lstrip()メソッドで左の改行や空白を削除
            return ''.join(title).lstrip()
        return title
    
    #priceを整形する関数
    def get_price(self,price):
        if price:
            #取得したpriceをstrからint型にする
            #str型のpriceをreplace()メソッドで置換する
            return int(price.replace('¥','').replace(',',''))
        return 0
    
    #sizeを整形する関数
    def get_size(self,size):
        if size:
            #lstripで左端のサイズと全角スペースを削除
            #splitで区切り文字'／(全角スラッシュ)'を起点にリスト化してリスト番号0を取得
            #教材と違い版は残す(教材はreplaceを使っている)
            return size.lstrip('サイズ ').split('／')[0]#.rstrip('判')
        return size
    
    #pageを整形する関数
    def get_page(self,page):
        if page:
            #str->int型に変更する
            #余計なページ数と全角スペースとpをreplaceで削除
            return int(page.split('／')[1].replace('ページ数 ','').replace('p',''))
        return 0
    
    #isbn(商品コード)を整形する関数
    def get_isbn(self,isbn):
        if isbn:
            #要らない商品コードと全角スペースを削除
            return isbn.replace('商品コード ','')
        return isbn

    #ジェネレーター関数なので実質ここからスタート
    def parse_item(self, response):
        #ログの出力。urlを返す
        # logging.info(response.url)
        
        ####ここでencodingの処理を行うと良いでしょう###
        
        #Item Loaderを使ってItemに格納する
        #第一引数にitems.pyのクラス名、第二引数に目的のWebページのレスポンス
        loader = ItemLoader(item=BookItem(),response=response)
        
        #loaderにadd_xpathメソッドでタイトルと取得するXPathを指定
        # add_xpath()で第一引数にites.pyのフィールド,第二引数でXPathを指定
        #CSSセレクターの場合はadd_css()メソッドを使う
        loader.add_xpath('title', '//h3[@itemprop="name"]/text()'),
        loader.add_xpath('author', '//div[@class="infobox ml10 mt10"]/ul/li[1]/a/text()'),
        loader.add_xpath('publisher', '//a[contains(@href,"publisher-key")]/text()'),
        loader.add_xpath('price', '//span[@class="sale_price"]/text()'),
        loader.add_xpath('size', 'normalize-space(//div[@class="infbox dotted ml10 mt05 pt05"]/ul/li[1]/text())'),
        loader.add_xpath('page', '//div[@class="infbox dotted ml10 mt05 pt05"]/ul/li[1]/text()'),
        loader.add_xpath('ISBN', '//div[@class="infbox dotted ml10 mt05 pt05"]/ul/li[2]/text()'),
        #fieldエラーを出す様に設定
        # loader.add_xpath('isbn', '//div[@class="infbox dotted ml10 mt05 pt05"]/ul/li[2]/text()'),
        #pipelineの実験ISBNが取得できなければpipelines.pyのraiseが発動する
        # loader.add_xpath('ISBN', '//h2/div[@class="infbox dotted ml10 mt05 pt05"]/ul/li[2]/text()'),
        
        # items.pyで定義したimage_urlsのフィールドとXPathを指定
        # loader.add_xpath('image_urls','//img[@itemprop="image"]/@src') # 相対URLで指定しているので画像は取得出来ない
        # xpath = r'img[@itemprop="image"]/@src'
        # loader.add_xpath('image_urls',f"https://www.kinokuniya.co.jp/{xpath}")
        # response.xpathでXPathを取得(今回は相対URLパス)。
        x_path = '//img[@itemprop="image"]/@src'
        x_path = response.xpath(x_path).get()
        # 相対パスを表示(検証用)
        # print('------------')
        # print(x_path)
        # print('------------')
        # 相対パスなので絶対パスに変換。
        # loader.add_xpathではXPathでの指定なので整形した絶対パスはエラーになる
        # loader.add_xpath('image_urls', f"https://www.kinokuniya.co.jp/{x_path.replace('../','')}")
        # loader.add_valueでフィールドに整形した絶対パスを指定
        loader.add_value('image_urls', f"https://www.kinokuniya.co.jp/{x_path.replace('../','')}")
        
        #load_item()メソッドでItemに最終的に格納する
        yield loader.load_item()
        
        #Itemを使う前の使用
        # yield {
        #     #getall()で全てのタイトルを取得
        #     #最初はリスト形式なので.strip()は使えない
        #     #get_title関数でXPathで指定したタイトルをstrで取得
        #     'title': self.get_title(response.xpath('//h3[@itemprop="name"]/text()').getall()),
        #     'author': response.xpath('//div[@class="infobox ml10 mt10"]/ul/li[1]/a/text()').get(),
        #     #こちらでは何故か要素を取得できないのでcontainsの方を採用
        #     # 'publisher': response.xpath('//div[@class="infobox ml10 mt10"]/ul/li[@class="newColor"]/a/text()').get(),
        #     'publisher': response.xpath('//a[contains(@href,"publisher-key")]/text()').get(),
        #     'price': self.get_price(response.xpath('//span[@class="sale_price"]/text()').get()),
        #     #サイズ・ページは後でデータを加工する
        #     #normalize-space()で余分な空白や段落を削除
        #     'size': self.get_size(response.xpath('normalize-space(//div[@class="infbox dotted ml10 mt05 pt05"]/ul/li[1]/text())').get()),
        #     #.strip()で余計な空白や段落を削除(CSSセレクタならこちらで)
        #     'page': self.get_page(response.xpath('//div[@class="infbox dotted ml10 mt05 pt05"]/ul/li[1]/text()').get().strip()),
        #     'ISBN': self.get_isbn(response.xpath('//div[@class="infbox dotted ml10 mt05 pt05"]/ul/li[2]/text()').get()),
        # }