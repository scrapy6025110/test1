#SQLite3からデータを取得

import sqlite3

# 接続系
db_path = 'open.db'
db = sqlite3.connect(db_path)
c = db.cursor()

# 全てのレコードをリスト化
sql = 'select * from computer_books;'
c.execute(sql)
lists = []
lists = c.fetchall()

# 表示
for i in lists:
    print(i)

# もちろん各種ファイルに書くのも可能
