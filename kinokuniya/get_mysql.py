# MySQLサーバーからデータを取得してCSVファイルを出力するファイル

import csv,datetime
import mysql.connector

# MySQLへの接続
db = mysql.connector.connect(
    user = 'root',
    password = 'example',
    host = 'mysql',
    database = 'mysql_scrapydb',
    charset = 'utf8mb4'
)
c = db.cursor()

# MySQLからデータを格納
# #* sql文を帰る事によりデータの加工・抽出、フィルタリング等が出来る *#
sql = 'select * from computer_books;'
c.execute(sql)
lists = []
lists = c.fetchall()

# CSVファイルの指定と展開
file_path = 'from_get_mysql.csv'
with open(file_path,mode='a',newline='',encoding='utf8') as f:
    writer = csv.writer(f)
    for i in lists:
        writer.writerow(i)
        
# 作業の終了
print(f"MySQLのデータベースmysql_scrapydbからテーブルcomputer_booksのデータを取得しました//{datetime.datetime.now()}")

db.commit()
db.close()