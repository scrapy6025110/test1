# scrapy crawl XXXX -o xxx.csvコマンドラインで
# 取得したデータをMySQLにUTF-8MB4で格納してみる

import csv
import mysql.connector

# MySQLへの接続設定
db = mysql.connector.connect(
    user='root',
    password='example',
    host='mysql',
    database='mysql_scrapydb',
    charset='utf8mb4'
)
cursor = db.cursor()

# テーブル作成（既に作成済みの場合はスキップ）
create_table_query = '''
CREATE TABLE IF NOT EXISTS computer_books (
    title VARCHAR(128) CHARACTER SET utf8mb4,
    author VARCHAR(128) CHARACTER SET utf8mb4,
    price INT,
    publisher VARCHAR(128) CHARACTER SET utf8mb4,
    size VARCHAR(128) CHARACTER SET utf8mb4,
    page INT,
    isbn VARCHAR(128) CHARACTER SET utf8mb4,
    PRIMARY KEY (isbn)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
'''
cursor.execute(create_table_query)
db.commit()

# scrapy crawl XXXX(スパイダー名) -o xxx.csvのxxx.csvファイルパスを指定
file_path = 'tmp.csv'
# CSVファイルを読み込み、データをMySQLにインポート
with open(file_path, mode='r', newline='', encoding='utf8') as file:
    reader = csv.reader(file)
    next(reader)  # ヘッダーをスキップ
    # isbnのキーがユニークでなくともデータベースに格納
    # MySQLのテーブルが空ならこれでも良い
    # for row in reader:
    #     cursor.execute(
    #         'INSERT INTO computer_books (title, author, price, publisher, size, page, isbn) VALUES (%s, %s, %s, %s, %s, %s, %s)',
    #         # row
    #         # CSVファイルのヘッダーの順序を合わせる
    #         (row[6], row[1], row[3], row[4], row[5], row[2], row[0])
    #     )
    # isbnがユニークものだけMySQLに格納する
    # # 主キー制約を一時的に削除
    # cursor.execute('ALTER TABLE computer_books DROP PRIMARY KEY')

    # PRIMARY KEYが存在するかチェック
    cursor.execute("SHOW INDEX FROM computer_books WHERE Key_name = 'PRIMARY'")
    indices = cursor.fetchall()

    if indices:
        # PRIMARY KEYが存在する場合、削除
        cursor.execute('ALTER TABLE computer_books DROP PRIMARY KEY')
    
    # データ挿入
    for row in reader:
        # priceカラムが空文字列の場合にNoneを代入
        price = int(row[3]) if row[3].isdigit() else None
        page = int(row[2]) if row[2].isdigit() else None
        
        # デバッグプリント
        print(f"Processing row: {row}")
        print(f"Title: {row[6]}, Author: {row[1]}, Price: {price}, Publisher: {row[4]}, Size: {row[5]}, Page: {page}, ISBN: {row[0]}")

        try:
            cursor.execute(
                'INSERT INTO computer_books (title, author, price, publisher, size, page, isbn) VALUES (%s, %s, %s, %s, %s, %s, %s)',
                # CSVファイルのヘッダーの順序を合わせる
                (row[6], row[1], price, row[4], row[5], page, row[0])
            )
        except mysql.connector.Error as err:
            if err.errno == 1062:  # 重複エラー
                print(f"Duplicate entry for ISBN {row[0]}, skipping.")
            else:
                print(f"Error inserting row {row}: {err}")
        
        # cursor.execute(
        #     'INSERT INTO computer_books (title, author, price, publisher, size, page, isbn) VALUES (%s, %s, %s, %s, %s, %s, %s)', 
        #     (row[6], row[1], row[3], row[4], row[5], row[2], row[0])
        # )

    # 主キーを再設定    
    cursor.execute('ALTER TABLE computer_books ADD PRIMARY KEY (isbn)')

db.commit()
cursor.close()
db.close()
