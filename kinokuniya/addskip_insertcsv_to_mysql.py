# MySQLでのprimary key(isbn)の重複の場合
# そのレコードを追加しないコード

import csv,datetime
import mysql.connector

# MySQLへの接続設定
db = mysql.connector.connect(
    user='root',
    password='example',
    host='mysql',
    database='mysql_scrapydb',
    charset='utf8mb4'
)
c = db.cursor()


# テーブル作成（既に作成済みの場合はスキップ）
create_table_query = '''
CREATE TABLE IF NOT EXISTS computer_books (
    title VARCHAR(128) CHARACTER SET utf8mb4,
    author VARCHAR(128) CHARACTER SET utf8mb4,
    price INT,
    publisher VARCHAR(128) CHARACTER SET utf8mb4,
    size VARCHAR(128) CHARACTER SET utf8mb4,
    page INT,
    isbn VARCHAR(128) CHARACTER SET utf8mb4,
    PRIMARY KEY (isbn)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
'''
c.execute(create_table_query)
db.commit()

# CSVファイルを開く
file_path = 'tmp.csv'
with open(file_path, 'r',newline='',encoding='utf8') as csvfile:
    csvreader = csv.reader(csvfile)
    
    # ヘッダー行をスキップ
    next(csvreader)
    
    # CSVファイルの各行をループ
    for row in csvreader:
        # CSVファイルのヘッダーの順序とMySQLのカラム番号を合わせる
        # ISBN,author,page,price,publisher,size,title(CSV)
        row[6], row[1], row[5], row[2], row[3], row[4], row[0] = row
        # isbn,author,page,price,publisher,size,title = row
        price = int(row[2]) if row[2].isdigit() else None
        page = int(row[5]) if row[5].isdigit() else None
        
        # レコードを挿入
        try:
            # 第二引数でレコードの順番を合わせる
            c.execute("INSERT INTO computer_books VALUES (%s, %s,%s, %s,%s, %s,%s)", (row[0], row[1], price, row[3], row[4], page, row[6]))
        except mysql.connector.IntegrityError as err:
            # 重複キーの場合はスキップ
            if err.errno == 1062:
                continue
            else:
                raise
# 成功したら表示を出す
print(f"成功しました//{datetime.datetime.now()}")

# 変更をコミット
db.commit()

# データベースを閉じる
db.close()